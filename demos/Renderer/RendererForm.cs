using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Utilify.Framework;
using Utilify.Framework.UI;
using Utilify.Platform.Configuration;
using Utilify.Platform.Demo.RendererLibrary;
using System.Net;

namespace Utilify.Platform.Demo.Renderer
{
    /// <summary>
    /// Summary description for RendererForm.
    /// </summary>
    public class RendererForm : Form
    {
        private PictureBox pictureBox1;
        private Button render;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;

        private int imageWidth = 0;
        private int imageHeight = 0;
        private int columns = 0;
        private int rows = 0;
        private int segmentWidth = 0;
        private int segmentHeight = 0;

        private Utilify.Framework.Application yafApp = null;
        private bool initted = false;

        private Bitmap composite = null;
        private bool drawnFirstSegment = false;

        private ComboBox widthCombo;
        private ComboBox heightCombo;
        private NumericUpDown columnsUpDown;
        private NumericUpDown rowsUpDown;
        private Label label5;
        private ComboBox sceneCombo;
        private Button stop;
        private CheckBox stretchCheckBox;

        // Create a logger for use in this class
        private static log4net.ILog log = log4net.LogManager.GetLogger(typeof(RendererForm));
        Utilify.Platform.Logger logger;

        private ProgressBar pbar;
        private Label lbProgress;
        private CheckBox renderLocalCheckBox;
        private string localAppDir;
        List<BackgroundWorker> localWorkerThreads;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        public RendererForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //initialise the Utilify framework logger
            Logger.MessageLogged += new EventHandler<LogEventArgs>(Logger_MessageLogged);
            logger = new Utilify.Platform.Logger();
        }

        //captures all framework log messages
        static void Logger_MessageLogged(object sender, LogEventArgs e)
        {
            string format = "[{0}] <{1}:{2}> {3} - {4} {5}";
            switch (e.Level)
            {
                case LogLevel.Debug:
                    log.DebugFormat(format,
                        e.Level,
                        e.StackFrame.GetFileName(), e.StackFrame.GetFileLineNumber(), e.StackFrame.GetMethod().Name,
                        e.Message, e.Exception);
                    break;
                case LogLevel.Info:
                    log.InfoFormat(format,
                        e.Level,
                        e.StackFrame.GetFileName(), e.StackFrame.GetFileLineNumber(), e.StackFrame.GetMethod().Name,
                        e.Message, e.Exception);
                    break;
                case LogLevel.Warn:
                case LogLevel.Error:
                    log.ErrorFormat(format,
                        e.Level,
                        e.StackFrame.GetFileName(), e.StackFrame.GetFileLineNumber(), e.StackFrame.GetMethod().Name,
                        e.Message, e.Exception);
                    break;
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.render = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.widthCombo = new System.Windows.Forms.ComboBox();
            this.heightCombo = new System.Windows.Forms.ComboBox();
            this.columnsUpDown = new System.Windows.Forms.NumericUpDown();
            this.rowsUpDown = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.sceneCombo = new System.Windows.Forms.ComboBox();
            this.stop = new System.Windows.Forms.Button();
            this.stretchCheckBox = new System.Windows.Forms.CheckBox();
            this.pbar = new System.Windows.Forms.ProgressBar();
            this.lbProgress = new System.Windows.Forms.Label();
            this.renderLocalCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnsUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowsUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.Location = new System.Drawing.Point(8, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(608, 480);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // render
            // 
            this.render.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.render.Location = new System.Drawing.Point(488, 536);
            this.render.Name = "render";
            this.render.Size = new System.Drawing.Size(120, 32);
            this.render.TabIndex = 5;
            this.render.Text = "render";
            this.render.Click += new System.EventHandler(this.render_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.Location = new System.Drawing.Point(8, 536);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 23);
            this.label1.TabIndex = 6;
            this.label1.Text = "width";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.Location = new System.Drawing.Point(8, 576);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 23);
            this.label2.TabIndex = 7;
            this.label2.Text = "height";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.Location = new System.Drawing.Point(176, 536);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 23);
            this.label3.TabIndex = 8;
            this.label3.Text = "columns";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.Location = new System.Drawing.Point(176, 576);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 23);
            this.label4.TabIndex = 9;
            this.label4.Text = "rows";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // widthCombo
            // 
            this.widthCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.widthCombo.Location = new System.Drawing.Point(72, 536);
            this.widthCombo.Name = "widthCombo";
            this.widthCombo.Size = new System.Drawing.Size(80, 21);
            this.widthCombo.TabIndex = 10;
            this.widthCombo.Text = "width";
            // 
            // heightCombo
            // 
            this.heightCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.heightCombo.Location = new System.Drawing.Point(72, 576);
            this.heightCombo.Name = "heightCombo";
            this.heightCombo.Size = new System.Drawing.Size(80, 21);
            this.heightCombo.TabIndex = 11;
            this.heightCombo.Text = "height";
            // 
            // columnsUpDown
            // 
            this.columnsUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.columnsUpDown.Location = new System.Drawing.Point(240, 536);
            this.columnsUpDown.Name = "columnsUpDown";
            this.columnsUpDown.Size = new System.Drawing.Size(56, 20);
            this.columnsUpDown.TabIndex = 12;
            // 
            // rowsUpDown
            // 
            this.rowsUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rowsUpDown.Location = new System.Drawing.Point(240, 576);
            this.rowsUpDown.Name = "rowsUpDown";
            this.rowsUpDown.Size = new System.Drawing.Size(56, 20);
            this.rowsUpDown.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.Location = new System.Drawing.Point(320, 536);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 23);
            this.label5.TabIndex = 14;
            this.label5.Text = "scene";
            // 
            // sceneCombo
            // 
            this.sceneCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.sceneCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sceneCombo.Location = new System.Drawing.Point(360, 536);
            this.sceneCombo.Name = "sceneCombo";
            this.sceneCombo.Size = new System.Drawing.Size(121, 21);
            this.sceneCombo.TabIndex = 15;
            // 
            // stop
            // 
            this.stop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.stop.Enabled = false;
            this.stop.Location = new System.Drawing.Point(488, 576);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(120, 32);
            this.stop.TabIndex = 16;
            this.stop.Text = "stop";
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // stretchCheckBox
            // 
            this.stretchCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.stretchCheckBox.Location = new System.Drawing.Point(360, 563);
            this.stretchCheckBox.Name = "stretchCheckBox";
            this.stretchCheckBox.Size = new System.Drawing.Size(104, 24);
            this.stretchCheckBox.TabIndex = 18;
            this.stretchCheckBox.Text = "stretch image";
            this.stretchCheckBox.CheckedChanged += new System.EventHandler(this.stretchCheckBox_CheckedChanged);
            // 
            // pbar
            // 
            this.pbar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pbar.Location = new System.Drawing.Point(192, 496);
            this.pbar.Name = "pbar";
            this.pbar.Size = new System.Drawing.Size(424, 23);
            this.pbar.TabIndex = 19;
            // 
            // lbProgress
            // 
            this.lbProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbProgress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbProgress.Location = new System.Drawing.Point(8, 496);
            this.lbProgress.Name = "lbProgress";
            this.lbProgress.Size = new System.Drawing.Size(176, 23);
            this.lbProgress.TabIndex = 20;
            this.lbProgress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // renderLocalCheckBox
            // 
            this.renderLocalCheckBox.AutoSize = true;
            this.renderLocalCheckBox.Location = new System.Drawing.Point(360, 591);
            this.renderLocalCheckBox.Name = "renderLocalCheckBox";
            this.renderLocalCheckBox.Size = new System.Drawing.Size(88, 17);
            this.renderLocalCheckBox.TabIndex = 21;
            this.renderLocalCheckBox.Text = "render locally";
            this.renderLocalCheckBox.UseVisualStyleBackColor = true;
            // 
            // RendererForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(624, 613);
            this.Controls.Add(this.renderLocalCheckBox);
            this.Controls.Add(this.lbProgress);
            this.Controls.Add(this.pbar);
            this.Controls.Add(this.stretchCheckBox);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.sceneCombo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rowsUpDown);
            this.Controls.Add(this.columnsUpDown);
            this.Controls.Add(this.heightCombo);
            this.Controls.Add(this.widthCombo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.render);
            this.Controls.Add(this.pictureBox1);
            this.Name = "RendererForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Utilify Renderer";
            this.Load += new System.EventHandler(this.RenderForm_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.RendererForm_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnsUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowsUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        //[STAThread]
        //static void Main()
        //{
        //    Application.Run(new RendererForm());
        //}

        #region Init Stuff

        private void RenderForm_Load(object sender, EventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(DefaultErrorHandler);
            //for windows forms apps unhandled exceptions on the main thread
            System.Windows.Forms.Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);

            widthCombo.Items.AddRange(new object[] { "100", "160", "200", "240", "400", "480", "512", "600", "640", "800", "1024", "1280" });
            heightCombo.Items.AddRange(new object[] { "100", "120", "200", "300", "320", "384", "480", "600", "640", "768", "800", "1024" });
            widthCombo.SelectedIndex = 4;
            heightCombo.SelectedIndex = 3;

            columnsUpDown.Value = 4;
            columnsUpDown.Maximum = 100;
            columnsUpDown.Minimum = 1;

            rowsUpDown.Value = 3;
            rowsUpDown.Maximum = 100;
            rowsUpDown.Minimum = 1;

            // Read model names from filesystem based on available sub-directories.
            string[] scenes = Directory.GetDirectories(".");
            //for (int i = 0; i < scenes.Length; i++)
            //    scenes[i] = scenes[i].Substring(2);
            sceneCombo.Items.AddRange(scenes);
            sceneCombo.SelectedIndex = 0;
        }

        private void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            HandleAllUnknownErrors(sender.ToString(), e.Exception);
        }

        private void DefaultErrorHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            HandleAllUnknownErrors(sender.ToString(), e);
        }

        private void HandleAllUnknownErrors(string sender, Exception e)
        {
            logger.Error("Unknown Error from: " + sender, e);
            MessageBox.Show(e.ToString(), "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void showSplash()
        {
            //ResourceManager resources = new ResourceManager(typeof(RendererForm));
            pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
            // Pick utilify logo here.
            //pictureBox1.Image = (Image)(resources.GetObject("pictureBox1.Image"));
        }

        #endregion

        private void clearImage()
        {
            if (imageWidth > 0 && imageHeight > 0)
            {
                composite = new Bitmap(imageWidth, imageHeight);
                if (stretchCheckBox.Checked)
                {
                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                else
                {
                    pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
                }
                pictureBox1.Image = composite;
            }
        }

        private void displayImage(Bitmap segment, int col, int row)
        {
            if (!drawnFirstSegment)
            {
                clearImage();
                drawnFirstSegment = true;
            }

            Graphics g = Graphics.FromImage(composite);
            Rectangle sourceRectangle;
            Rectangle destRectangle;
            int x = 0;
            int y = 0;
            x = col * segmentWidth;
            y = row * segmentHeight;

            logger.Debug("Displaying segment c, r: " + col + ", " + row);
            try
            {
                sourceRectangle = new Rectangle(0, 0, segment.Width, segment.Height);
                destRectangle = new Rectangle(x, y, segment.Width, segment.Height);
                g.DrawImage(segment, destRectangle, sourceRectangle, GraphicsUnit.Pixel);
            }
            catch (Exception e)
            {
                logger.Debug("!!!ERROR:\n" + e.StackTrace);
            }
            pictureBox1.Image = composite;

        }

        private void render_Click(object sender, EventArgs e)
        {
            stop.Enabled = true;
            render.Enabled = !stop.Enabled;

            drawnFirstSegment = false;
            showSplash();

            // get width and height from combo box
            imageWidth = Int32.Parse(widthCombo.SelectedItem.ToString());
            imageHeight = Int32.Parse(heightCombo.SelectedItem.ToString());

            // get cols and rows from up downs
            columns = Decimal.ToInt32(columnsUpDown.Value);
            rows = Decimal.ToInt32(rowsUpDown.Value);

            segmentWidth = imageWidth / columns;
            segmentHeight = imageHeight / rows;

            logger.Debug("WIDTH:" + imageWidth);
            logger.Debug("HEIGHT:" + imageHeight);
            logger.Debug("COLUMNS:" + columns);
            logger.Debug("ROWS:" + rows);

            // reset the display
            clearImage();

            // set progress bar
            pbar.Maximum = columns * rows;
            pbar.Minimum = 0;
            pbar.Value = 0;
            lbProgress.Text = "";
           
            if (this.renderLocalCheckBox.Checked)
            {
                // Do nothing for now.
                LocalRender();
            }
            else
            {
                UtilifyRender();
            }
        }

        #region Local Render

        private void LocalRender()
        {
            //RUN Local!

            // Create Local App Dir
            DirectoryInfo localAppDirInfo = Directory.CreateDirectory(Guid.NewGuid().ToString());
            localAppDir = localAppDirInfo.FullName;

            // Read all files in selected scene directory
            FileInfo yafrayXMLFile = null;
            string sceneDirectory = sceneCombo.SelectedItem.ToString();
            logger.Debug("Reading scene files for : " + sceneDirectory);

            string[] sceneFiles = Directory.GetFiles(sceneDirectory);
            foreach (string sceneFile in sceneFiles)
            {
                logger.Debug("Found file : " + sceneFile);
                FileInfo info = new FileInfo(sceneFile);
                // identify the yafray scene XML file
                if (info.Extension.Equals(".xml"))
                {
                    yafrayXMLFile = info;
                    logger.Debug("Found Yafray XML file : " + yafrayXMLFile.Name);
                    // Substitute output resolution (in a real application we probably wouldnt be changing resolution here?)
                    SubstituteResolutionYafray(imageWidth, imageHeight, yafrayXMLFile);
                }
                
                // copy each scene file to the app directory
                info.CopyTo(Path.Combine(localAppDirInfo.FullName, info.Name));
            }

            logger.Debug("Creating local Jobs...");

            localWorkerThreads = new List<BackgroundWorker>();
            for (int x = 0; x < columns; x++)
            {
                for (int y = 0; y < rows; y++)
                {
                    UtilifyYafrayJob yafJob =
                        new UtilifyYafrayJob(yafrayXMLFile.Name, imageWidth, imageHeight, segmentWidth, segmentHeight, x, y);

                    LocalExecutionHelper.ProvideExecutionEventHandler(yafJob);

                    BackgroundWorker localWorker = new BackgroundWorker();
                    localWorker.DoWork +=
                        new DoWorkEventHandler(localWorker_DoWork);
                    localWorker.RunWorkerCompleted +=
                        new RunWorkerCompletedEventHandler(localWorker_RunWorkerCompleted);

                    localWorker.RunWorkerAsync(yafJob);
                    localWorkerThreads.Add(localWorker);
                }
            }
        }

        private void localWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            UtilifyYafrayJob job = (UtilifyYafrayJob)e.Argument;
            // create new directory for job
            DirectoryInfo dir = Directory.CreateDirectory(Path.Combine(localAppDir, Guid.NewGuid().ToString()));
            // copy scene files
            job.Render(dir.FullName);
            e.Result = job;
        }

        private void localWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            logger.Debug("Job finished: ");
            logger.Debug("Processing Job...");
            processJob(((UtilifyYafrayJob)e.Result));
        }

        #endregion
        
        #region Utilify Render

        private void UtilifyRender()
        {
            // RUN Utilify!

            // Read all files in selected scene directory
            FileInfo yafrayXMLFile = null;
            string sceneDirectory = sceneCombo.SelectedItem.ToString();
            logger.Debug("Reading scene files for : " + sceneDirectory);

            List<DependencyInfo> dependencies = new List<DependencyInfo>();
            string[] sceneFiles = Directory.GetFiles(sceneDirectory);
            foreach (string sceneFile in sceneFiles)
            {
                logger.Debug("Found file : " + sceneFile);
                FileInfo info = new FileInfo(sceneFile);
                dependencies.Add(new DependencyInfo(info.Name, info.FullName, DependencyType.DataFile));
                // identify the yafray scene XML file
                if (info.Extension.Equals(".xml"))
                {
                    yafrayXMLFile = info;
                    logger.Debug("Found Yafray XML file : " + yafrayXMLFile.Name);
                }
            }

            // Substitute output resolution (in a real application we probably wouldnt be changing resolution here?)
            SubstituteResolutionYafray(imageWidth, imageHeight, yafrayXMLFile);

            logger.Debug("Creating App and Jobs...");

            // Create Username Credentials to be used during AuthN and AuthZ
            LoginForm login = new LoginForm();
            DialogResult result = login.ShowDialog(this);
            ConnectionSettings settings = null;
            if (result == DialogResult.OK)
            {
                settings = login.ConnectionSettings;
            }
            else
            {
                return;
            }

            if (yafApp != null && yafApp.Status != ApplicationStatus.Stopped)
                yafApp.Cancel();

            yafApp =
                new Utilify.Framework.Application("Yafray Render Application", null, dependencies, settings);

            for (int x = 0; x < columns; x++)
            {
                for (int y = 0; y < rows; y++)
                {
                    UtilifyYafrayJob yafJob =
                        new UtilifyYafrayJob(yafrayXMLFile.Name, imageWidth, imageHeight, segmentWidth, segmentHeight, x, y);
                    yafApp.AddJob(yafJob);
                }
            }

            yafApp.Error += new EventHandler<Utilify.Framework.ErrorEventArgs>(yafApp_Error);
            yafApp.JobCompleted += new EventHandler<JobCompletedEventArgs>(yafApp_JobCompleted);

            logger.Debug("Starting App...");
            yafApp.Start();

        }

        private void yafApp_Error(object sender, Utilify.Framework.ErrorEventArgs e)
        {
            logger.Debug("Job failed: " + e.Error.Message);
            UpdateStatus();
        }

        private void yafApp_JobCompleted(object sender, JobCompletedEventArgs e)
        {
            logger.Debug("Job finished: " + e.Job.Id);
            logger.Debug("Processing Job...");
            UtilifyYafrayJob job = (UtilifyYafrayJob)e.Job.CompletedInstance;
            processJob(job);
        }

        #endregion


        #region Resolution Stuff

        private void SubstituteResolutionYafray(int imageWidth, int imageHeight, FileInfo yafrayXMLFile)
        {
            // move orig. file
            string origPath = yafrayXMLFile.FullName;
            string tmpPath = Path.Combine(yafrayXMLFile.Directory.FullName, yafrayXMLFile.Name + "~");

            // if ~ file is still there then last time we ran something failed, so recover that file as the original file.
            if (File.Exists(tmpPath))
            {
                File.Copy(tmpPath, origPath, true);
                File.Delete(tmpPath);
            }

            // Copy original file
            yafrayXMLFile.CopyTo(tmpPath, true);

            // put this in a try/catch too and if something fails revert to tmp copy.

            // open orig. file to read
            using (StreamReader sr = File.OpenText(tmpPath))
            {
                using (StreamWriter sw = File.CreateText(origPath))
                {
                    // read orig. file and write out each line to the new file
                    // replace the resx and resy in new file
                    string line = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Contains("resx"))
                        {
                            line = ReplaceResX(line, imageWidth);
                        }
                        if (line.Contains("resy"))
                        {
                            line = ReplaceResY(line, imageHeight);
                        }
                        sw.WriteLine(line);
                    }
                }
            }

            // delete tmp file.
            File.Delete(tmpPath);
        }

        private string ReplaceResY(string line, int imageHeight)
        {
            // replace the value for resy
            int resyIndex = line.IndexOf("resy");
            int firstQuoteIndex = line.IndexOf("\"", resyIndex);
            int lastQuoteIndex = line.IndexOf("\"", firstQuoteIndex + 1); // +1 is after the opening quote
            string newLine = line.Substring(0, firstQuoteIndex + 1) + imageHeight + line.Substring(lastQuoteIndex);
            return newLine;
        }

        private string ReplaceResX(string line, int imageWidth)
        {
            // replace the value for resx
            int resxIndex = line.IndexOf("resx");
            int firstQuoteIndex = line.IndexOf("\"", resxIndex);
            int lastQuoteIndex = line.IndexOf("\"", firstQuoteIndex + 1); // +1 is after the opening quote
            string newLine = line.Substring(0, firstQuoteIndex + 1) + imageWidth + line.Substring(lastQuoteIndex);
            return newLine;
        }

        #endregion

        #region Process Job Stuff

        private void processJob(UtilifyYafrayJob job)
        {
            logger.Debug("Processing Job. col=" + job.Column + " row=" + job.Row);
            if (job != null)
            {
                if (job.ImageSegment != null)
                    logger.Debug("Image Size: width=" + job.ImageSegment.Size.Width + " height= " + job.ImageSegment.Size.Height);
                Bitmap bmp = job.ImageSegment;
                if (bmp != null)
                {
                    logger.Debug("Loading from bitmap");
                    displayImage(bmp, job.Column, job.Row);
                }
                else
                {
                    logger.Debug("bmp is null! col=" + job.Column + " row=" + job.Row);
                }
            }

            // Update Status
            this.Invoke(new UpdateStatusDelegate(UpdateStatus));
        }

        #region Update Status

        // Delegate method to update status
        private void UpdateStatus()
        {
            if (pbar.Value < pbar.Maximum)
            {
                pbar.Increment(1);
                lbProgress.Text = string.Format("Job {0} of {1} completed.", pbar.Value, pbar.Maximum);
            }
            // check if that was the last job.
            if (pbar.Value == pbar.Maximum)
            {
                MessageBox.Show("Rendering Finished");
                stop.Enabled = false;
                render.Enabled = !stop.Enabled;
                lbProgress.Text = string.Format("All {0} Jobs completed.", pbar.Maximum);
                if (yafApp != null && yafApp.Status != ApplicationStatus.Stopped)
                    yafApp.Cancel();
            }
        }

        // The delegate
        private delegate void UpdateStatusDelegate();

        #endregion

        #endregion

        private void stop_Click(object sender, EventArgs e)
        {
            StopApp();
        }

        private void StopApp()
        {
            try
            {
                if (yafApp != null && yafApp.Status != ApplicationStatus.Stopped)
                {
                    yafApp.Cancel();
                    logger.Debug("Application stopped.");
                }
                else
                {
                    if (yafApp == null)
                    {
                        logger.Debug("yafApp is null");
                    }
                    else
                    {
                        logger.Debug("yafApp is Stopped...");
                    }
                }

                if (localWorkerThreads != null)
                {
                    foreach (BackgroundWorker worker in localWorkerThreads)
                    {
                        // todoLater: Not sure how to kill the local threads yet...
                        worker.WorkerSupportsCancellation = true;
                        worker.CancelAsync();
                        worker.Dispose();
                    }
                }

                stop.Enabled = false;
                render.Enabled = !stop.Enabled;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error stopping application: " + ex.Message);
            }
        }

        private void stretchCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (stretchCheckBox.Checked)
            {
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else
            {
                pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
            }
        }

        private void RendererForm_Closing(object sender, CancelEventArgs e)
        {
            StopApp();
        }

    }
}
