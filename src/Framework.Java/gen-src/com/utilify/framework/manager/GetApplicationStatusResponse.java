
package com.utilify.framework.manager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.client.ApplicationStatusInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetApplicationStatusResult" type="{http://schemas.utilify.com/2007/09/Client}ApplicationStatusInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getApplicationStatusResult"
})
@XmlRootElement(name = "GetApplicationStatusResponse")
public class GetApplicationStatusResponse {

    @XmlElement(name = "GetApplicationStatusResult", nillable = true)
    protected ApplicationStatusInfo getApplicationStatusResult;

    /**
     * Gets the value of the getApplicationStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationStatusInfo }
     *     
     */
    public ApplicationStatusInfo getGetApplicationStatusResult() {
        return getApplicationStatusResult;
    }

    /**
     * Sets the value of the getApplicationStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationStatusInfo }
     *     
     */
    public void setGetApplicationStatusResult(ApplicationStatusInfo value) {
        this.getApplicationStatusResult = value;
    }

}
