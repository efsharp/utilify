
package com.utilify.framework;

/**
 * Internal utility class used to generate a sequence of numbers.
 */
public final class NumberSequenceGenerator {

    private Object syncLock = new Object();
    private long sequence = 0;

    /**
     * Gets the next number in sequence.
     * This method is thread-safe.
     * @return the next number in sequence.
     */
    public long GetNext()
    {
        synchronized (syncLock)
        {
            if (sequence < Long.MAX_VALUE)
                sequence++;
            else
                sequence = 0; //reset the sequence
        }
        return sequence;
    }

    /**
     * Gets the next sequence number.
     * The numbers generated are 64-bit integers.
     * This method is thread-safe.
     * @return the next number in sequence (converted to a <code>String</code>)
     */
    public String GetNextSequence()
    {
        return "" + GetNext();
    }
    
}
