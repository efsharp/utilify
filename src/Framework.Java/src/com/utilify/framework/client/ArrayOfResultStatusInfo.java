
package com.utilify.framework.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for ArrayOfResultStatusInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfResultStatusInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultStatusInfo" type="{http://schemas.utilify.com/2007/09/Client}ResultStatusInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Wrapper class to represent a list of {@link ResultStatusInfo} objects.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfResultStatusInfo", propOrder = {
    "resultStatusInfos"
})
public class ArrayOfResultStatusInfo {

    @XmlElement(name = "ResultStatusInfo", nillable = true)
    private List<ResultStatusInfo> resultStatusInfos;

    /**
     * Creates an instance of the <code>ArrayOfResultStatusInfo</code> class with an empty list of result statuses.
     */
    public ArrayOfResultStatusInfo(){
    	resultStatusInfos = new ArrayList<ResultStatusInfo>();
    }
    
    /**
     * Creates an instance of the <code>ArrayOfResultStatusInfo</code> class with the specified collection of results statuses.
     * @param statuses
     * 	the collection of result statuses.
     */
    public ArrayOfResultStatusInfo(Collection<ResultStatusInfo> statuses){
    	this();
    	if (statuses != null){
    		resultStatusInfos.addAll(statuses);
    	}
    }
    
    /**
     * Creates an instance of the <code>ArrayOfResultStatusInfo</code> class with the specified array of results statuses.
     * @param statuses
     * 	the array of result statuses.
     */
    public ArrayOfResultStatusInfo(ResultStatusInfo[] statuses){
    	this();
    	if (statuses != null){
    		for (int i = 0; i < statuses.length; i++) {
    			resultStatusInfos.add(statuses[i]);
			}
    	}
    }

    /**
     * Gets the list of result statuses.
     * @return
     *	an array of <code>ResultStatusInfo</code> instances.
     */    
    public ResultStatusInfo[] toArray(){
    	ResultStatusInfo[] statuses = new ResultStatusInfo[resultStatusInfos.size()];
    	for (int i = 0; i < statuses.length; i++) {
			statuses[i] = resultStatusInfos.get(i);
		}
    	return statuses;
    }    
}
