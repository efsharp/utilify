
package com.utilify.framework.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.utilify.framework.ErrorMessage;
import com.utilify.framework.ExecutionContext;
import com.utilify.framework.FrameworkException;
import com.utilify.framework.ResultRetrievalMode;

/**
 * The interface that specifies that the implementing class is a native executable job.
 * The class that implements this interface declares the (relative) path of the executable that is run on the 
 * remote machine as part of the job execution. 
 * If a class implements the <code>NativeExecutable</code> interface, 
 * the entry point (implementation of {@link Executable#execute(ExecutionContext)} is not called - the native executable returned by
 * the {@link #getExecutablePath()} is run instead.
 */
public final class NativeExecutable implements Executable, Dependent, Results {
	
	private String executablePath;
    private String arguments;
    private String stdOutFile = "stdout.txt";
    private String stdErrorFile = "stderr.txt";
    private List<DependencyInfo> dependencies = new ArrayList<DependencyInfo>();
    private List<ResultInfo> results = new ArrayList<ResultInfo>();
    
    private NativeExecutable() {}

    public NativeExecutable(String executablePath) {
    	this();
        setExecutablePath(executablePath);
    }

    public NativeExecutable(String executablePath, String arguments) {
    	this(executablePath);
    	if (arguments != null)
    		setArguments(arguments);
    }

    public NativeExecutable(String executablePath, String arguments, Collection<DependencyInfo> dependencies,
        Collection<ResultInfo> expectedResults) {
    	this(executablePath, arguments);
        if (dependencies != null)
            this.dependencies.addAll(dependencies);
        if (results != null)
            this.results.addAll(results);
    }

    public String getExecutablePath() {
    	return executablePath;
    }
    
    public void setExecutablePath(String executablePath) {
        if (executablePath == null || executablePath.trim().length() == 0) 
            throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + " : executablePath");
        this.executablePath = executablePath;
    }

    public String getArguments() {
    	return arguments;
    }
    
    public void setArguments(String arguments) {
        if (arguments == null)
            throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + " : arguments");
        this.arguments = arguments;
    }

    public String getStdOutFile() {
    	return stdOutFile;
    }
    
	public void setStdOutFile(String stdOutFile) {
		this.stdOutFile = stdOutFile;
	}

    public String getStdErrorFile() {
    	return stdErrorFile;
    }
    
    public void setStdErrorFile(String stdErrorFile) {
    	this.stdErrorFile = stdErrorFile;
    }
    
    // Executable Interface Methods
	public void execute(ExecutionContext context) throws Exception {
		throw new FrameworkException("The 'execute' method is not supported for NativeExecutable.");
	}
	// End Executable Interface Methods
	
	// Dependent Interface Methods
	public DependencyInfo[] getDependencies() {
		return dependencies.toArray(new DependencyInfo[] {});
	}
	// End Dependent Interface Methods
	
	// Results Interface Methods
	public ResultInfo[] getExpectedResults() {
        List<ResultInfo> resultsCopy = new ArrayList<ResultInfo>(results);

        if (stdOutFile != null && stdOutFile.trim().length() > 0)
            resultsCopy.add(new ResultInfo(stdOutFile, ResultRetrievalMode.BACK_TO_ORIGIN));

        if (stdErrorFile != null && stdErrorFile.trim().length() > 0)
            resultsCopy.add(new ResultInfo(stdErrorFile, ResultRetrievalMode.BACK_TO_ORIGIN));

        return resultsCopy.toArray(new ResultInfo[] {});	
    }
	// End Results Interface Methods
}
