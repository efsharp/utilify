
package com.utilify.framework.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for ArrayOfJobStatusInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfJobStatusInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="JobStatusInfo" type="{http://schemas.utilify.com/2007/09/Client}JobStatusInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Wrapper class to represent a list of {@link JobStatusInfo} objects. 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfJobStatusInfo", propOrder = {
    "jobStatusInfos"
})
public class ArrayOfJobStatusInfo {

    @XmlElement(name = "JobStatusInfo", nillable = true)
    private List<JobStatusInfo> jobStatusInfos;

    /**
     * Creates an instance of the <code>ArrayOfJobStatusInfo</code> class with an empty list of job statuses.
     */
    public ArrayOfJobStatusInfo(){
    	jobStatusInfos = new ArrayList<JobStatusInfo>();
    }
    
    /**
     * Creates an instance of the <code>ArrayOfJobStatusInfo</code> class with with the specified collection of job statuses.
     * @param statuses
     * 	the collection of statuses to add.
     */
    public ArrayOfJobStatusInfo(Collection<JobStatusInfo> statuses){
    	this();
    	if (statuses != null){
    		jobStatusInfos.addAll(statuses);
    	}
    }
    
    /**
     * Creates an instance of the <code>ArrayOfJobStatusInfo</code> class with with the specified array of job statuses.
     * @param statuses
     *	the array of statuses to add.
     */
    public ArrayOfJobStatusInfo(JobStatusInfo[] statuses){
    	this();
    	if (statuses != null){
    		for (int i = 0; i < statuses.length; i++) {
    			jobStatusInfos.add(statuses[i]);
			}
    	}
    }

    /**
     * Gets the list of job statuses.
     * @return
     * 	an array of <code>JobStatusInfo</code> instances 
     */    
    public JobStatusInfo[] toArray(){
    	JobStatusInfo[] statuses = new JobStatusInfo[jobStatusInfos.size()];
    	for (int i = 0; i < statuses.length; i++) {
			statuses[i] = jobStatusInfos.get(i);
		}
    	return statuses;
    }
}
