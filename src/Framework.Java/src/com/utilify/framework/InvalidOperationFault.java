
package com.utilify.framework;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for InvalidOperationFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvalidOperationFault">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents a SOAP fault that occurs when an operation is called at an inappropriate time. 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = InvalidOperationFault.FaultName, propOrder = {
    "message"
})
public class InvalidOperationFault {

	/**
	 * The name of the fault (as represented in the XML Schema)
	 */
	public static final String FaultName = "InvalidOperationFault";
		
    @XmlElement(name = "Message", required = true, nillable = true)
    private String message;

    /**
     * Gets the fault message.
     * @return the fault message  
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the fault message.
     * @param value - the fault message   
     */
    @SuppressWarnings("unused")
	private void setMessage(String value) {
        this.message = value;
    }

}
