
package com.utilify.framework;

import javax.xml.ws.WebFault;

/**
 * Thrown when an unexpected internal exception occurs on the server.
 */
@WebFault(name = ServerFault.FaultName, targetNamespace = Namespaces.Framework)
public class InternalServerException extends RuntimeException {

	private static final long serialVersionUID = -6780663145335146516L;
	
	/*
     * Java type that goes as soapenv:Fault detail element.
     */
    private ServerFault faultInfo;

    //needed for JAXWS to figure out if this is the wrapper class for a declared fault
    /**
     * Gets the SOAP fault that caused this exception.
     * @return the associated {@link ServerFault}.
     */
    public ServerFault getFaultInfo(){
    	return faultInfo;
    }

	/**
	 * Constructs an InternalServerException with no detail message.
	 */
	private InternalServerException(){
    	super();
    }
    /**
	 * Constructs an InternalServerException with the specified detail message.
	 * @param message - the detail message.
     */
    public InternalServerException(String message){
    	super(message);
    }
    /**
	 * Constructs an InternalServerException with the specified detail message and fault.
	 * @param message - the detail message.
	 * @param faultInfo - the fault object that is the cause of this exception.
     */
    public InternalServerException(String message, ServerFault faultInfo){
    	super(message);
    	this.faultInfo = faultInfo;
    }
    /**
	 * Constructs an InternalServerException with the specified detail message and cause.
	 * @param message - the detail message.
	 * @param cause - the cause of the exception.
     */
    public InternalServerException(String message, Throwable cause){
    	super(message, cause);
    }
}