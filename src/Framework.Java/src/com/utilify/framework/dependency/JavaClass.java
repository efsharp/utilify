package com.utilify.framework.dependency;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * The <code>JavaClass</code> class represents a Java 
 * class.
 */
/*
	Adapted from:
 @author <b>Mike Clark</b>
 @author Clarkware Consulting, Inc.

 */
class JavaClass {

	private ClassBuilder builder;
    private String className;
    private String packageName;
    private File source;
    
    private ArrayList<String> referencedClasses = new ArrayList<String>();
    
    /**
     * Creates an instance of the <code>JavaClass</code> class with the specified name.
     * @param name
     * 	the name of the class.
     * @param builder 
     */
    JavaClass(String name, ClassBuilder builder) {
        this.className = name;
        this.builder = builder;
        this.packageName = className.substring(0, className.lastIndexOf('.'));
    }

    /**
     * @return a list of referenced class names
     */
    String[] getReferencedClasses(){
    	return referencedClasses.toArray(new String[referencedClasses.size()]);
    }
    
    void addReferencedClass(String class1){
    	if (class1 != null && !referencedClasses.contains(class1))
    		referencedClasses.add(class1);
    }

    /**
     * Gets the class name.
     * @return
     * 	the name of the class.
     */
    String getName() {
        return className;
    }

    /**
     * Gets the package name.
     * @return the name of the package
     */
    String getPackageName() {
    	return packageName;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object other) {

        if (other instanceof JavaClass) {
            JavaClass otherClass = (JavaClass) other;
            return otherClass.getName().equals(getName());
        }

        return false;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return getName().hashCode();
    }

	/**
	 * @return the classFile
	 */
	File getSource() {
		return source;
	}
	void setSource(File source){
		if (source != null)
			this.source = source;
	}

	/**
	 * Gets the path of parent directory of the package containing this class 
	 * @return
	 * 	the parent directory path.
	 */
	 String getClassDirectory(){
		//calculate the parent dir where the package begins
		String classDir = null;
		String absPath = (source == null) ? null : source.getAbsolutePath();
		if (absPath != null){
			/*
				abs path would be something like: 
					/adirectory/moredirs/com/utilify/framework/TheClass.class
			 */
			System.out.println("Pageage is " + getPackageName());
			String packageDir = getPackageName().replace('.', File.separatorChar);
			System.out.println("Package dir is " + packageDir);
			/*
			  	Package dir would be: 
			  		com/utilify/framework 
			 */
			if (absPath.indexOf(".") >= 0)
				classDir = absPath.substring(0, absPath.indexOf(packageDir));
			else
				classDir = ".";
		}
		return classDir;
	}
	
	boolean isInJar(){
		return (source != null && source.isFile() && source.getName().endsWith(".jar"));
	}

	/**
	 * @return the builder
	 */
	ClassBuilder getBuilder() {
		return builder;
	}

	/**
	 * A comparator to compare two {@link JavaClass} instances.
	 */
	public static class ClassComparator implements Comparator {

        /**
         * Compares the <code>JavaClass</code> instances by name.
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        public int compare(Object a, Object b) {
            JavaClass c1 = (JavaClass) a;
            JavaClass c2 = (JavaClass) b;

            return c1.getName().compareTo(c2.getName());
        }
    }

}
