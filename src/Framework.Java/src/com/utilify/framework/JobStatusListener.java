
package com.utilify.framework;

import com.utilify.framework.client.Application;

/**
 * The listener interface for receiving job status events. 
 * The class that is interested in processing a job status event implements this interface, 
 * and the object created with that class is registered with an {@link Application}, 
 * using it's {@link Application#addStatusListener} method. When the job status event occurs, 
 * that object's {@link #onJobStatusChanged} method is invoked. 
 */
public interface JobStatusListener {
	/**
	 * Invoked when a job's status changes. 
	 * @param event - the event object the contains the status information.
	 */
	void onJobStatusChanged(JobStatusEvent event);
}
