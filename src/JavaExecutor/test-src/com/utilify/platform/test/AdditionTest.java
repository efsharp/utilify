package com.utilify.platform.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.utilify.framework.ApplicationInitializationException;
import com.utilify.framework.ErrorEvent;
import com.utilify.framework.ErrorListener;
import com.utilify.framework.JobCompletedEvent;
import com.utilify.framework.JobCompletedListener;
import com.utilify.framework.JobStatusEvent;
import com.utilify.framework.JobStatusListener;
import com.utilify.framework.client.Application;
import com.utilify.framework.client.Job;

public class AdditionTest implements JobCompletedListener, JobStatusListener, ErrorListener {

    public static void main(String[] args) throws IOException, 
    	ApplicationInitializationException, InterruptedException {

        AdditionTest prog = new AdditionTest();
        prog.test();
        
        System.out.println("Press enter to exit...");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
    }

    private void test() throws ApplicationInitializationException, InterruptedException{
    	System.out.println("Starting client api test...");

    	// Create a new Application
        Application app = new Application("My Addition Application");

        // Create Jobs and add to the Application
        for (int i = 1; i < 10; i++) {
            AdditionJob job = new AdditionJob(i, i);
            app.addJob(job);
        }

        // Add listeners for errors and completed jobs.
        app.addErrorListener(this);
        app.addStatusListener(this);
        app.addJobCompletedListener(this);
        
        // Start the Application
        System.out.println("Submitting Application");
        app.start();

        System.out.println("Waiting for results..." + this);
    }

    // Error event handler
	public void onError(ErrorEvent event) {
		System.out.println("An Error Occurred: \n" +  event.getException().toString());
		if (event.getException().getCause() != null)
			System.out.print("Cause: ");
			event.getException().getCause().printStackTrace();
	}

	// Job completed event handler
	public void onJobCompleted(JobCompletedEvent event) {
		
		Job remoteJob = event.getJob();
		
		// Recreate the Addition Job and print out results
        AdditionJob myJob = (AdditionJob)remoteJob.getCompletedInstance();       
        String s = String.format("Job %s Completed in %d ms. %d + %d = %d",
                        remoteJob.getId(),
                        (event.getJob().getFinishTime().getTime() - remoteJob.getStartTime().getTime()),
                        myJob.getValueA(),
                        myJob.getValueB(),
                        myJob.getResult());
        System.out.println(s);
	}

	// Job status changed event handler
	public void onJobStatusChanged(JobStatusEvent event) {
		System.out.println("Job status changed to " + event.getStatusInfo().getStatus());
	}
}
