package com.utilify.platform.executor;

import com.utilify.framework.StringAppender;

public class ExecutionCompleteEventArgs {

	private String jobId;
	private String applicationId;
	private byte[] finalJobInstance;
	private StringAppender executionLog;
	private StringAppender executionError;
	private Exception exception;
	private ExecutionCompleteEventArgs() {
	}

	public ExecutionCompleteEventArgs(String jobId, String applicationId,
			byte[] finalJobInstance, StringAppender executionLog,
			StringAppender executionError, Exception exception) {
		setJobId(jobId);
		setApplicationId(applicationId);
		setFinalJobInstance(finalJobInstance);
		setExecutionLog(executionLog);
		setExecutionError(executionError);
		setException(exception);
	}

	public String getJobId() {
		return jobId;
	}

	private void setJobId(String value) {
		jobId = value;
	}

	public String getApplicationId() {
		return applicationId;
	}

	private void setApplicationId(String value) {
		applicationId = value;
	}

	public byte[] getFinalJobInstance() {
		return finalJobInstance;
	}

	private void setFinalJobInstance(byte[] value) {
		finalJobInstance = value;
	}

	public StringAppender getExecutionLog() {
		return executionLog;
	}

	private void setExecutionLog(StringAppender value) {
		executionLog = value;
	}

	public StringAppender getExecutionError() {
		return executionError;
	}

	private void setExecutionError(StringAppender value) {
		executionError = value;
	}

	public Exception getException() {
		return exception;
	}

	private void setException(Exception value) {
		exception = value;
	}
}