package com.utilify.platform.executor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.utilify.framework.client.JobCompletionInfo;

public class JobCompletionMessageData implements MessageData {

	private JobCompletionInfo[] infos;

	public JobCompletionMessageData() {}
	
	public JobCompletionMessageData(JobCompletionInfo[] infos) {
		this.infos = infos;
	}
		
	public byte[] toBytes() throws IOException {
		// For each JobCompletionInfo we need to create the bytes
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		for (JobCompletionInfo info : infos) {
			MessageDataHelper.writeNextBytes(stream, jobCompletionInfoToBytes(info));
		}
		return stream.toByteArray();
	}
	
//	private JobCompletionInfo jobCompletionInfoFromBytes(byte[] bytes) throws IOException {
//		ByteArrayInputStream stream = new ByteArrayInputStream(bytes);
//		String jobId = MessageDataHelper.readNextLine(stream);
//	    String applicationId = MessageDataHelper.readNextLine(stream);
//	    byte[] jobInstance = MessageDataHelper.readNextBytes(stream);
//	    String executionLog = new String(MessageDataHelper.readNextBytes(stream), MessageDataHelper.CHARSET);
//	    String executionError = new String(MessageDataHelper.readNextBytes(stream), MessageDataHelper.CHARSET);    
//
//	    JobCompletionInfo info = new JobCompletionInfo(jobId, applicationId, jobInstance,
//	    										executionLog, executionError);
//	    return info;
//	}
	
	private byte[] jobCompletionInfoToBytes(JobCompletionInfo info) throws IOException {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		MessageDataHelper.writeLine(stream, info.getJobId());
		MessageDataHelper.writeLine(stream, info.getApplicationId());
		MessageDataHelper.writeNextBytes(stream, info.getJobInstance());
		MessageDataHelper.writeNextBytes(stream, info.getExecutionLog().getBytes(MessageDataHelper.CHARSET));
		MessageDataHelper.writeNextBytes(stream, info.getExecutionError().getBytes(MessageDataHelper.CHARSET));	    
	    return stream.toByteArray();		
	}

	public boolean isValid() {
		return true;
	}

	public JobCompletionInfo[] getJobs() {
		return infos;
	}
}
