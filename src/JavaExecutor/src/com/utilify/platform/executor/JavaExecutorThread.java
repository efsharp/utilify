package com.utilify.platform.executor;

import java.lang.reflect.Method;

class JavaExecutorThread implements Runnable {

	private Object executor;
	private JobExecutionInfo execInfo;
	
	private JavaExecutorThread(){}
	
	JavaExecutorThread(Object executor, JobExecutionInfo execInfo){
		if (executor == null)
			throw new IllegalArgumentException("JvmExecutor instance cannot be null");
		if (execInfo == null)
			throw new IllegalArgumentException("Execution info cannot be null");
		this.executor = executor;
		this.execInfo = execInfo;
	}
	
	public void run() {
		executeTask();
	}

	//call the JvmExecutor's executeTask method using reflection
	void executeTask(){
		try {
			Method executeTask = this.executor.getClass().getMethod("executeTask", new Class[] { JobExecutionInfo.class });
			executeTask.invoke(this.executor, new Object[] { this.execInfo });
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
}
