
namespace Utilify.Platform.Execution.Shared
{
    public enum MessageType
    {
        Unknown = 0,
        ExecuteJob = 1,
        GetCompletedJobs = 2,
        CompletedJobs = 3,
        AbortJob = 4,
        Shutdown = 5
    }
}
