using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using Utilify.Framework;
using Utilify.Platform.Execution.Shared;

namespace Utilify.Platform.Execution
{
    public class NativeExecutor : MarshalByRefObject, IExecutor
    {
        Logger logger = new Logger();

        #region IExecutor Members

        public event EventHandler<ExecutionCompleteEventArgs> ExecutionComplete;

        public void ExecuteTask(object info)
        {
            JobExecutionInfo execInfo = (JobExecutionInfo)info;

            //create the ExecutionContext
            ExecutionContext context = ExecutionContext.Create(execInfo.JobId, execInfo.JobDirectory,
                new StringAppender(), new StringAppender());

            ExecuteTaskWithContext(execInfo, context); //.JobId, execInfo.ApplicationId, execInfo.JobInstance, execInfo.JobDirectory);
            
            context = null;
        }

        #endregion

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "We need to catch any exceptions caused by executing user code, so that it can be passed back to the user.")]
        private void ExecuteTaskWithContext(JobExecutionInfo execInfo, ExecutionContext context)
        {
            Exception executionException = null;
            byte[] finalJobInstance = null;
            StringAppender executionLog = context.Log;
            StringAppender executionError = context.Error;
            DateTime start = DateTime.Now; //default
            DateTime finish = DateTime.MaxValue; //default

            try
            {
                if (execInfo == null)
                    throw new ArgumentNullException("execInfo");

                if (execInfo.JobInstance == null || execInfo.JobInstance.Length == 0)
                    throw new ArgumentException("Job instance cannot be null or empty", "execInfo");
                if (string.IsNullOrEmpty(execInfo.JobDirectory))
                    throw new ArgumentException("Job directory cannot be null or empty", "execInfo");

                //before running anything - just set the final instance to be the same as the initial. So that we don't ever have a null instance.
                finalJobInstance = execInfo.JobInstance;

                executionLog.AppendLine("Environment Current Directory: " + Environment.CurrentDirectory);
                executionLog.AppendLine("Execution Context WorkingDirectory: " + context.WorkingDirectory);
                executionLog.AppendLine("AppDomain FriendlyName: " + AppDomain.CurrentDomain.FriendlyName);
                executionLog.AppendLine("AppDomain BaseDirectory: " + AppDomain.CurrentDomain.BaseDirectory);
                executionLog.AppendLine("AppDomain DynamicDirectory: " + AppDomain.CurrentDomain.DynamicDirectory);
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                executionLog.AppendLine("Assemblies Loaded (NativeExecutor): " + assemblies.Length);
                foreach (Assembly asm in assemblies)
                {
                    executionLog.AppendLine("Assembly (NativeExecutor): " + asm.GetName().Name);
                }

                // Deserialise the Job and cast to IExecutable
                executionLog.AppendLine("Deserialising job...");
                object deserialisedJob = SerializationHelper.Deserialize(execInfo.JobInstance);
                NativeExecutable exec = deserialisedJob as NativeExecutable;
                if (exec == null)
                    throw new ArgumentException("Job is not an instance of NativeExecutable");

                exec.ExecutionStarted += new EventHandler<ExecutionEventArgs>(NativeExecutable_ExecutionStarted);

                executionLog.AppendLine("Deserialised job. Executing...");
                logger.Debug("Executing Job " + execInfo.JobId + " (NativeExecutor)");

                // Execute Job
                start = DateTime.Now;
                exec.Execute(context);
                finish = DateTime.Now;

                executionLog.AppendLine("Executed job. Serialising it again...");

                // Remove the listener! otherwise it will try to serialize it too!
                exec.RemoveListeners();

                // Serialise the completed Job
                byte[] serialisedJob = SerializationHelper.Serialize(exec);
                executionLog.AppendLine("Serialized job. Returning final instance...");
                // Assign the final instance
                finalJobInstance = serialisedJob;
            }
            catch (Exception e)
            {
                if (finish == DateTime.MaxValue)
                    finish = DateTime.Now; // incase we blow up before executing the method
                executionError.AppendLine("Error executing job: " + e.StackTrace);
                executionException = e;
            }
            finally
            {
                byte[] executionExceptionInstance = null;
                try
                {
                    executionExceptionInstance = SerializationHelper.Serialize(executionException);
                }
                catch (Exception ex)
                {
                    executionError.AppendLine("Could not serialize execution exception because of error : " + ex.Message);
                    executionError.AppendLine("Exection excpetion stack trace follows: ");
                    executionError.AppendLine(executionException.ToString());
                }

                long cpuTime = (long)((finish - start).TotalMilliseconds);

                // Raise Completed Event.
                ExecutionCompleteEventArgs completeArgs =
                    new ExecutionCompleteEventArgs(
                    execInfo.JobId,
                    execInfo.ApplicationId,
                    finalJobInstance,
                    executionLog,
                    executionError,
                    executionExceptionInstance,
                    cpuTime);

                EventHelper.RaiseEvent<ExecutionCompleteEventArgs>(ExecutionComplete, this, completeArgs);
            }
        }

        void NativeExecutable_ExecutionStarted(object sender, ExecutionEventArgs e)
        {
            // Execute Job
            RunProcess(e.ExecutionArgs);
        }

        private void RunProcess(ExecutionArgs executionArgs)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            String exePath = executionArgs.ExecutablePath;
            if (exePath.Trim().Length == 0)
                return;

            exePath = exePath.Replace('\\', '/');
            // Check for an absolute path. If its not, then append the context working dir.
            if (exePath.IndexOf(':') <= 0 && !exePath.StartsWith("/"))
                exePath = Path.Combine(executionArgs.WorkingDirectory, executionArgs.ExecutablePath);
            
            startInfo.FileName = exePath;

            if (executionArgs.Arguments != null)
                startInfo.Arguments = executionArgs.Arguments;

            logger.Debug("Executing command: " + startInfo.FileName + " " + startInfo.Arguments);

            startInfo.CreateNoWindow = true;
            startInfo.WorkingDirectory = executionArgs.WorkingDirectory;
            startInfo.UseShellExecute = false;

            Process proc = new Process();
            proc.StartInfo = startInfo;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.RedirectStandardError = true;
            proc.Start();

            proc.WaitForExit();

            // todo: this should be made to handle large files.
            String stdOutPath = Path.Combine(executionArgs.WorkingDirectory, executionArgs.StdOutFile);
            using (FileStream fs = File.Create(stdOutPath)) { }
            logger.Debug("Creating StdOut: " + stdOutPath);
            using (FileStream fs = File.OpenWrite(stdOutPath))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    String line = proc.StandardOutput.ReadToEnd();
                    if (line != null && line.Trim().Length > 0)
                        sw.WriteLine(line);
                }
            }

            String stdErrPath = Path.Combine(executionArgs.WorkingDirectory, executionArgs.StdErrFile);
            using (FileStream fs = File.Create(stdErrPath)) { }
            logger.Debug("Creating StdErr: " + stdErrPath);
            using (FileStream fs = File.OpenWrite(stdErrPath))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    String line = proc.StandardError.ReadToEnd();
                    if (line != null && line.Trim().Length > 0)
                        sw.WriteLine(line);
                }
            }

            proc.Close();
        }
    }
}
