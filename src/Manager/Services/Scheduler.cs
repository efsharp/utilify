using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using Utilify.Platform.Manager.Persistence;
using PersistentExecutor = Utilify.Platform.Manager.Persistence.Executor;

namespace Utilify.Platform.Manager.Services
{
    //todoLater: Refactor scheduler to include core functions such as :
    //1. Mapping, upmapping jobs (to be overridable using custom logic)
    //2. Check for apps, jobs to ready
    //3. Check for jobs to complete
    //4. Add and check for jobs stuck at an Executor, perhaps re-schedule to another one.
    // #4 is not yet implemented here. Perhaps #2,3 can be pulled into a seperate JobMonitor class that is a seperate PlatformServiceBase-thread
    internal class Scheduler : PlatformServiceBase
    {
        //not using the back-off helper / base class interval
        private int interval = 1000; //milliseconds

        //We use automatic versioning using the ORM, to have some higher-level locking for the application, 
        //so that multiple threads don't update the status at the same time
        //using different detached instances of the same app

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "We catch the general Exception here, since this is a seperate thread, and we don't want to abruptly kill it. We log the error and raise an event.")]
        protected override void Run()
        {
            try
            {
                int mappedInThisRound = 0;
                int resetThisRound = 0;
                int readiedThisRound = 0;

                while (!IsStopRequested)
                {
                    //reset counters
                    mappedInThisRound = 0;
                    resetThisRound = 0;
                    readiedThisRound = 0;

                    DateTime loopStartTime = DateTime.Now;

                    //0. Look at previously mapped jobs, and see it anyones's been stuck too long  - and 'unmap it'
                    //so it will be picked up for scheduling later on
                    //when doing the budget stuff the unmapping should reclaim the previously allocated budget - since it was not used
                    resetThisRound = ResetJobs();

                    //1. Update status of Apps & Jobs that are ready to be scheduled.
                    readiedThisRound = UpdateAppsToReady();

                    #region 2. Get Applications to schedule, pull Executors, and Schedule 'ready' Jobs from selected Apps.

                    //2. pull apps to schedule and executors which are free
                    IList<PersistentExecutor> executors = GetFreeExecutors();
                    IList<Application> apps = GetApplicationsToSchedule();
                    int freeSlotCount = CountTotalFreeSlots(executors); // total number of slots available in the system/network
                    int numJobsPerApp = CalculateNumJobsPerApp(freeSlotCount, apps.Count);

                    if (freeSlotCount > 0)
                    {
                        logger.Debug("Got {0} free slots.", freeSlotCount);

                        //1.1. using the list of apps which are ready to be scheduled,
                        //we round-robin applications submitting 'n' jobs from each, so that other apps with jobs also get a fair chance
                        if (apps.Count > 0)
                        {
                            logger.Debug("Got {0} apps to schedule.", apps.Count);
                            logger.Debug("Scheduling ~{0} Jobs Per App.", numJobsPerApp);
                        }

                        foreach (Application app in apps)
                        {
                            //1.2. for each app, pull jobs which are ready to be scheduled
                            IList<Job> jobs = GetJobsToSchedule(app, numJobsPerApp);
                            if (jobs.Count > 0)
                                logger.Debug("Got {0} jobs to schedule for app {1}", jobs.Count, app);

                            foreach (Job job in jobs)
                            {
                                logger.Debug("Mapping job {0} to executor {1} ({2})", job.Id.ToString(), executors[0].Uri, executors[0].Id.ToString());

                                // From the list of Executors, Map the Job to the first Executor that the Job isnt already Mapped to.

                                //1.3. Map the job to the executor
                                PersistentExecutor selectedExecutor = null;
                                if (executors != null && executors.Count > 0)
                                    selectedExecutor = executors[0];

                                if (selectedExecutor != null)
                                {
                                    MapJobToExecutor(job, selectedExecutor);
                                    mappedInThisRound++;
                                    
                                    //1.4. refresh the executor list
                                    // Remove the selected executor from the list and add it to the end of the list ONLY if it still
                                    // has some free slots.
                                    executors.Remove(selectedExecutor);
                                    if (CountFreeSlots(selectedExecutor) > 0)
                                        executors.Add(selectedExecutor);

                                    freeSlotCount = CountTotalFreeSlots(executors);
                                    numJobsPerApp = CalculateNumJobsPerApp(freeSlotCount, apps.Count);
                                }
                                
                                //PersistentExecutor selectedExecutor = SelectExecutor(executors, job);
                                //if (selectedExecutor != null)
                                //{
                                //    MapJobToExecutor(job, selectedExecutor);
                                //    mappedInThisRound++;
                                //    //1.4. refresh the executor list
                                //    executors = RefreshExecutorList(executors, selectedExecutor); // THIS REFRESH CRASHES A LOT! (hibernate related)
                                //    freeSlotCount = CountFreeSlots(executors);
                                //    numJobsPerApp = CalculateNumJobsPerApp(freeSlotCount, apps.Count);                                    
                                //}
                            }

                            if (IsStopRequested)
                                break;
                        }
                    }
                    //else
                    //{
                    //    logger.Debug("No free executors found. Continuing...");
                    //}

                    #endregion

                    //3. Complete any finished Jobs
                    CompleteJobs();

                    if (IsStopRequested) //we could come out of the apps loop above because we were stopped. so better check here again
                        break;

                    //sleep only if we've not done any work this time around:
                    if (mappedInThisRound == 0 
                        && resetThisRound == 0 
                        && readiedThisRound == 0)
                        Sleep(interval);

#if DEBUG
                    //TimeSpan loopDuration = DateTime.Now - loopStartTime;
                    //logger.Debug("Loop finished in {0} ms. ", loopDuration.TotalMilliseconds);
                    //if (mappedInThisRound == 0 && resetThisRound == 0 && completedThisRound == 0)
                    //    logger.Debug("Slept for {0} ms", interval);
#endif

                }
            }
            catch (PersistenceException px)
            {
                logger.Warn("Error: " + px.StackTrace);
                OnError(px, true);
            }
            catch (ThreadAbortException)
            {
                Thread.ResetAbort();
                logger.Info("Scheduler thread aborted.");
            }
            catch (Exception ex)
            {
                //catch all other exceptions and let the manager-host know
                logger.Warn("Error:", ex);
                OnError(ex, true);
            }
            logger.Info("Scheduler thread ended.");
        }

        //This is not ideal: this method exists only so we can exclude it from profiling results.
        private void Sleep(int interval)
        {
            Thread.Sleep(interval);
        }

        private PersistentExecutor SelectExecutor(IList<PersistentExecutor> executors, Job job)
        {
            if (executors != null && executors.Count > 0)
            {
                // For Round-Robin scheduling to Executors, we just pick whoever is on top of the list. (ie. return executors[0];)

                // But, if the Executor already has a Mapping for this Job, then we don't want to give it to that Executor again.
                // If it already has a mapping it means there was some problem executing the Job on that Executor, and
                // the Executor is yet to come and clean up its Mappings.
                foreach (PersistentExecutor executor in executors)
                {
                    if (SchedulerDao.CheckJobMappingExists(executor.Id, job.Id))
                    {
                        logger.Debug("Not scheduling Job:" + job.Id + " to Executor:" + executor.Id + " because previous execution on this Executor failed or was reset");
                        continue;
                    }
                    else
                    {
                        return executor;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Sets the job status to 'completed' from 'completing' if all results have been sent.
        /// </summary>
        private int CompleteJobs()
        {
            int jobsCompleted = 0;
            //3. Now check for any Jobs that are Completing and have all their Results satisfied.
            //   These Jobs need to be set to Completed.

            // todoDiscuss: What to do with Jobs that remain in the Execute or Completing state for too long.
            IList<Job> jobsToComplete = SchedulerDao.GetJobsByStatus(JobStatus.Completing);
            if (jobsToComplete.Count > 0)
            {
                logger.Debug("Got {0} jobs to complete...", jobsToComplete.Count);
                foreach (Job jobToComplete in jobsToComplete)
                {
                    //this will check if the job can be set to completed and save it back if it is.
                    bool completed = CheckJobResults(jobToComplete);
                    logger.Debug("Checked job {0}, completed = {1}", jobToComplete.Id, completed);
                    if (completed)
                        jobsCompleted++;
                }
            }

            return jobsCompleted;
        }

        /// <summary>
        /// Reset any Jobs that are stuck
        /// </summary>
        private int ResetJobs()
        {
            int removedMappings = SchedulerDao.RemoveMappingsOlderThan(new TimeSpan(0, 0, 0, 0, interval * 2));
            if (removedMappings > 0)
                logger.Info("Removed {0} mappings because they were not picked up...", removedMappings);
            return removedMappings;
        }

        /// <summary>
        /// Checks dependencies (and qos) of apps with 'submitted' status and see if 
        /// they can be made 'ready' to schedule
        /// </summary>
        /// <returns></returns>
        private int UpdateAppsToReady()
        {
            int jobsReadied = 0;

            IList<Application> appsToCheck = SchedulerDao.GetApplicationsByStatus(ApplicationStatus.Submitted);
            if (appsToCheck.Count > 0)
            {
                logger.Debug("Got {0} applications to ready...", appsToCheck.Count);
                foreach (Application appToCheck in appsToCheck)
                {
                    bool ready = CheckApplicationDependencies(appToCheck); //this will check if it can be made ready, and will save it back
                    logger.Debug("Checked app {0} ({1}), ready = {2}", appToCheck.Name, appToCheck.Id, ready);
                }
            }
            //Ready Submitted Jobs : Check jobs that have 'submitted' status are ready to be given a 'ready' status.
            //Only jobs from 'ready' apps will be checked. 
            //Criteria for checking Job is that it is 'submitted' and belongs to a 'ready' Application (all are equal here).
            //Cannot use the list of Apps used above because status here needs to be 'ready' not 'submitted'.
            IList<Job> jobsToCheck = SchedulerDao.GetSubmittedJobsFromReadyApplications();
            if (jobsToCheck.Count > 0) //check to avoid calling into foreach.
            {
                foreach (Job jobToCheck in jobsToCheck)
                {
                    bool jobIsReady = CheckJobDependencies(jobToCheck);
                    logger.Debug("Checked job {0} from app {1}, jobIsReady = {2}",
                        jobToCheck.Id, jobToCheck.Application.Id, jobIsReady);
                    if (jobIsReady)
                        jobsReadied++;
                }
            }

            return jobsReadied;
        }

        private int CalculateNumJobsPerApp(int freeSlotCount, int appCount)
        {
            double num = (double)freeSlotCount / (double)appCount;
            return (int)Math.Ceiling(num);
        }

        private int CountTotalFreeSlots(IList<PersistentExecutor> executors)
        {
            long freeSlots = 0;
            foreach (PersistentExecutor exec in executors)
            {
                //considering twice the amount of free-slots,
                //since on average, on the executor side there would be one job being retrieved, 
                //while another is running,
                //while yet another could be sent back - all at the same time.              
                //freeSlots += (exec.Processors.Length * 2 - (exec.JobStatistics.ScheduledJobs + exec.JobStatistics.ExecutingJobs));
                freeSlots += CountFreeSlots(exec);
            }
            return (int)freeSlots;
        }

        private int CountFreeSlots(PersistentExecutor executor)
        {
            long freeSlots = 0;
            if (executor != null)
                freeSlots = (executor.Processors.Length - (executor.JobStatistics.ScheduledJobs + executor.JobStatistics.ExecutingJobs));
            return (int)freeSlots;
        }

        //After mapping the job, remove executor from list if it has no more free slots
        //Then re-sort the list (descending) by # of free slots
        private IList<PersistentExecutor> RefreshExecutorList(
            IList<PersistentExecutor> executors,
            PersistentExecutor executor)
        {
            int index = executors.IndexOf(executor);

            ////just refresh this executor from the db, to get the latest stats, and re-sort the list by free slots
            //PersistentExecutor updatedExec = SystemInfoDao.RefreshExecutor(executor);
            //executors[index] = updatedExec;

            ////now sort it by free slots DESC:
            //List<PersistentExecutor> listToSort = new List<PersistentExecutor>(executors);
            //listToSort.Sort(CompareByFreeSlots);

            // just get the list of free executors sorted by free slots
            IList<PersistentExecutor> newList = GetFreeExecutors();

            //then put the executor that was last scheduled to on the bottom of the list (for round robin)
            newList.Remove(executor);
            newList.Add(executor);

            return newList;
        }

        private int CompareByFreeSlots(PersistentExecutor exec1, PersistentExecutor exec2)
        {
            long freeSlots1 = exec1.Processors.Length - (exec1.JobStatistics.ScheduledJobs + exec1.JobStatistics.ExecutingJobs);
            long freeSlots2 = exec2.Processors.Length - (exec2.JobStatistics.ScheduledJobs + exec2.JobStatistics.ExecutingJobs);
            return freeSlots1.CompareTo(freeSlots2);
        }

        private bool MapJobToExecutor(Job job, PersistentExecutor executor)
        {
            //Issue 035: check in history if a job-mapping to this executor previously failed. check executor reputation etc.
            //add a record to the Mapping table. Add history. Update Job, Executor in the database
            long mappingId = 0;
            try
            {
                //to make sure it all happens in a single transaction
                mappingId = SchedulerDao.SaveJobMapping(job.Id, executor.Id);
            }
            catch (InvalidTransitionException itx)
            {
                logger.Info("Invalid transition exception while mapping job. Ignoring and continuing...", itx);
            }
            return (mappingId > 0);
            //Issue 036: need to update the jobs' app's Qos to reduce the remaining budget and other things.
        }

        //Issue 037: improve this query to check for executor limits and so on
        private IList<PersistentExecutor> GetFreeExecutors()
        {
            //get executors which have (mapped jobs + executing jobs) < # of CPUs
            //sort DESC by # of free slots (# of CPUs - (mapped + executing jobs))
            //which have contacted the manager in the last 2 mins
            IList<PersistentExecutor> execs = null;
            try
            {
                DateTime allowedLastPingTime = DateTime.UtcNow.Subtract(new TimeSpan(0, 2, 0));
                execs = SystemInfoDao.GetFreeExecutorsSortedByAvailableSlots(allowedLastPingTime);
            }
            catch (PersistenceException px)
            {
                logger.Warn("Error getting executors. Continuing with empty list...", px);
                execs = new List<PersistentExecutor>();
            }
            return execs;
        }

        private IList<Application> GetApplicationsToSchedule()
        {
            IList<Application> apps = null;
            //if an Application has status = Ready, that means its dependencies are already resolved.
            //We need apps which have:
            //status = Ready (all deps resolved),
            //qos.EarliestStartTime > DateTime.Now has passed (ignore budget for now),
            //statistics.ReadyJobs > 0,
            //Sorted by priority (DESC), then by latest job start time (ASC), then by submission time (ASC).
            apps = SchedulerDao.GetApplicationsToScheduleBasic();

            return apps;
        }

        private IList<Job> GetJobsToSchedule(Application application, int count)
        {
            IList<Job> jobs = null;
            
            //if an Job has status = Ready, that means its dependencies are already resolved.
            //We need jobs for the specified app where
            //status = Ready,
            //Sorted by submission time (ASC).
            jobs = SchedulerDao.GetJobsToScheduleBasic(application.Id, count);

            return jobs;
        }

        #region Check dependencies for apps, jobs and make them 'ready' - if possible

        private bool CheckApplicationDependencies(Utilify.Platform.Manager.Persistence.Application app)
        {
            bool waiting = false;
            try
            {
                //get the application and check if it has any unresolved dependencies.
                //based on that, update the status
                Dependency[] deps = app.GetDependencies(); //this shouldn't be null, but still we play safe
                if (deps != null)
                {
                    foreach (Dependency dep in deps)
                    {
                        if (dep != null)
                        {
                            if (!dep.IsResolved)
                            {
                                waiting = true; //if any of the app's dependencies is unresolved, it is set to the waiting status
                                break;
                            }
                        }
                    }
                }

                //we don't check qos here - to be consistent with a similar job-check: check for dependencies only
                //Qos qos = app.Qos;
                //if (DateTime.UtcNow <= qos.EarliestStartTime)
                //    waiting = true; //keep waiting if the earliest start time has not passed

                //update the status to Ready if the app's deps have been resolved
                if (!waiting)
                {
                    ApplicationDao.UpdateApplicationStatus(app.Id, ApplicationStatus.Ready);
                }
            }
            catch (InvalidTransitionException itx)
            {
                //ignore this : since it could be that the status can't change to ready, because someone already cancelled it
                logger.Info("Ignoring InvalidTransitionException: {0} for application {1}", itx.Message, app.Id); 
            }
            catch (PersistenceException pe)
            {
                logger.Error("Error checking application dependencies. Application = " + app, pe);
                waiting = true;
            }
            return !waiting; //return true, if the app is not waiting anymore : i.e it is now ready
        }

        private bool CheckJobDependencies(Utilify.Platform.Manager.Persistence.Job job)
        {
            bool waiting = false;
            try
            {

                //get the job and check if it has any unresolved dependencies.
                //based on that, update the status
                Dependency[] deps = job.GetDependencies(); //this shouldn't be null, but still we play safe
                if (deps != null)
                {
                    foreach (Dependency dep in deps)
                    {
                        if (dep != null)
                        {
                            if (!dep.IsResolved)
                            {
                                waiting = true; //if any of the job's dependencies is unresolved, it is set to the waiting status
                                break;
                            }
                        }
                    }
                }

                //update the status to Ready if the job's deps have been resolved
                if (!waiting)
                {
                    ApplicationDao.UpdateJobStatus(job.Id, JobStatus.Ready);
                }
            }
            catch (InvalidTransitionException itx)
            {
                //ignore this : since it could be that the status can't change to ready, because someone already cancelled it
                logger.Info("Ignoring InvalidTransitionException: {0} for job {1}", itx.Message, job.Id);
            }
            catch (PersistenceException pe)
            {
                logger.Error("Error checking job dependencies. Job = " + job, pe);
                waiting = true;
            }
            return !waiting; //return true if the job has been set to ready
        }

        #endregion

        #region Check results for jobs and make them 'complete' - if possible

        private bool CheckJobResults(Utilify.Platform.Manager.Persistence.Job job)
        {
            bool completing = false;
            try
            {

                //get the job and check if it has any unresolved results.
                //based on that, update the status
                Result[] results = job.GetResults(); //this shouldn't be null, but still we play safe
                if (results != null)
                {
                    foreach (Result result in results)
                    {
                        if (result != null)
                        {
                            if (!result.IsResolved)
                            {
                                //if any of the job's results are unresolved, it is set to the completing status
                                completing = true;
                                break;
                            }
                        }
                    }
                }

                //update the status to Complete if the job's results have been resolved
                if (!completing)
                {
                    ApplicationDao.UnmapJob(job.Id, JobStatus.Completed);
                }
            }
            catch (InvalidTransitionException itx)
            {
                //ignore this : since it could be that the status can't change to completed, because someone already cancelled it
                //or if the job was reset to be Ready / if it was Cancelled
                logger.Info("Ignoring InvalidTransitionException: {0} for job {1}", itx.Message, job.Id);
            }
            catch (PersistenceException pe)
            {
                logger.Error("Error checking job results. Job = " + job, pe);
                completing = true;
            }
            return !completing; //return true if the job has been set to completed
        }
        
        #endregion
        
        protected internal override string Name
        {
            get { return "Scheduler"; }
        }
    }
}
