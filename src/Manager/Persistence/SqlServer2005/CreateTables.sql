USE [UtilifyPlatform]
GO
/****** Object:  Table [dbo].[BinaryData]    Script Date: 10/25/2009 00:51:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BinaryData](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[value] [image] NOT NULL,
	[path] [nvarchar](2000) NULL,
 CONSTRAINT [PK_BinaryData] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Grp]    Script Date: 10/25/2009 00:51:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Grp](
	[grpId] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[grpId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 10/25/2009 00:51:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Permission](
	[permissionId] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[description] [varchar](255) NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[permissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Application]    Script Date: 10/25/2009 00:51:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Application](
	[id] [uniqueidentifier] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[clientHost] [varchar](255) NOT NULL,
	[username] [varchar](50) NOT NULL,
	[budget] [int] NOT NULL,
	[deadline] [bigint] NOT NULL,
	[earliestStartTime] [bigint] NOT NULL,
	[latestStartTime] [bigint] NOT NULL,
	[priority] [smallint] NOT NULL,
	[creationTime] [bigint] NOT NULL,
	[status] [smallint] NOT NULL CONSTRAINT [DF_Application_status]  DEFAULT ((0)),
	[version] [bigint] NOT NULL CONSTRAINT [DF_Application_version]  DEFAULT ((0)),
 CONSTRAINT [PK_Application] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Executor]    Script Date: 10/25/2009 00:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Executor](
	[id] [uniqueidentifier] NOT NULL,
	[uri] [nvarchar](255) NOT NULL,
	[dedicated] [bit] NOT NULL,
	[osName] [varchar](50) NOT NULL,
	[osVersion] [varchar](30) NOT NULL,
	[pingInterval] [int] NOT NULL,
	[lastPingTime] [bigint] NOT NULL,
	[version] [bigint] NOT NULL CONSTRAINT [DF_ExecutorConfiguration_version]  DEFAULT ((0)),
	[username] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Executor] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Job]    Script Date: 10/25/2009 00:51:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Job](
	[id] [uniqueidentifier] NOT NULL,
	[submissionTime] [bigint] NOT NULL,
	[startTime] [bigint] NULL,
	[status] [smallint] NOT NULL,
	[finishTime] [bigint] NULL,
	[applicationId] [uniqueidentifier] NOT NULL,
	[executorId] [uniqueidentifier] NULL,
	[type] [smallint] NOT NULL,
	[instanceDataId] [bigint] NULL,
	[version] [bigint] NOT NULL CONSTRAINT [DF_Job_version]  DEFAULT ((0)),
	[executionLog] [text] NULL,
	[executionError] [text] NULL,
	[initialInstanceDataId] [bigint] NULL,
	[exceptionInstanceDataId] [bigint] NULL,
	[platform] [smallint] NULL,
	[scheduledTime] [bigint] NULL,
	[cpuTime] [bigint] NULL,
	[mappedCount] [int] NOT NULL CONSTRAINT [DF_Job_mappedCount]  DEFAULT ((0)),
 CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dependency]    Script Date: 10/25/2009 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dependency](
	[id] [uniqueidentifier] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[filename] [varchar](1000) NOT NULL,
	[hash] [varchar](512) NULL,
	[modified] [bigint] NOT NULL,
	[size] [bigint] NOT NULL,
	[type] [smallint] NOT NULL,
	[cached] [bigint] NULL,
	[contentDataId] [bigint] NULL,
	[version] [bigint] NOT NULL CONSTRAINT [DF_Dependency_version]  DEFAULT ((0)),
 CONSTRAINT [PK_DependencyInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Grp_Permission]    Script Date: 10/25/2009 00:51:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Grp_Permission](
	[grpId] [int] NOT NULL,
	[permissionId] [int] NOT NULL,
	[arrayIndexColumn] [bigint] NULL,
 CONSTRAINT [PK_Grp_Permission] PRIMARY KEY CLUSTERED 
(
	[grpId] ASC,
	[permissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usr]    Script Date: 10/25/2009 00:51:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usr](
	[username] [varchar](50) NOT NULL,
	[password] [varchar](255) NOT NULL,
	[windowsUsername] [varchar](50) NULL,
	[certUsername] [varchar](255) NULL,
	[grpId] [int] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Application_Dependency]    Script Date: 10/25/2009 00:51:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Application_Dependency](
	[applicationId] [uniqueidentifier] NOT NULL,
	[dependencyId] [uniqueidentifier] NOT NULL,
	[arrayIndexColumn] [bigint] NULL,
 CONSTRAINT [PK_Application_Dependency] PRIMARY KEY CLUSTERED 
(
	[applicationId] ASC,
	[dependencyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Job_Dependency]    Script Date: 10/25/2009 00:51:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Job_Dependency](
	[jobId] [uniqueidentifier] NOT NULL,
	[dependencyId] [uniqueidentifier] NOT NULL,
	[arrayIndexColumn] [bigint] NULL,
 CONSTRAINT [PK_Job_Dependency] PRIMARY KEY CLUSTERED 
(
	[jobId] ASC,
	[dependencyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Memory]    Script Date: 10/25/2009 00:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Memory](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[sizeTotal] [bigint] NOT NULL,
	[sizeTotalFree] [bigint] NULL,
	[sizeUsageCurrent] [bigint] NULL,
	[sizeUsageLimit] [bigint] NULL,
	[executorId] [uniqueidentifier] NULL,
	[version] [bigint] NOT NULL CONSTRAINT [DF_Memory_version]  DEFAULT ((0)),
	[arrayIndexColumn] [int] NULL,
 CONSTRAINT [PK_Memory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DiskVolume]    Script Date: 10/25/2009 00:51:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DiskVolume](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[fileSystem] [varchar](20) NOT NULL,
	[root] [varchar](3) NOT NULL,
	[sizeTotal] [bigint] NOT NULL,
	[sizeTotalFree] [bigint] NULL,
	[sizeUsageCurrent] [bigint] NULL,
	[sizeUsageLimit] [bigint] NULL,
	[executorId] [uniqueidentifier] NULL,
	[arrayIndexColumn] [int] NULL,
	[version] [bigint] NOT NULL CONSTRAINT [DF_Disk_version]  DEFAULT ((0)),
 CONSTRAINT [PK_Disk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Processor]    Script Date: 10/25/2009 00:51:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Processor](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](100) NOT NULL,
	[vendor] [varchar](50) NOT NULL,
	[architecture] [varchar](20) NOT NULL,
	[speedMax] [decimal](18, 4) NOT NULL,
	[speedCurrent] [decimal](18, 4) NULL,
	[loadTotalCurrent] [decimal](18, 4) NULL,
	[loadUsageCurrent] [decimal](18, 4) NULL,
	[loadUsageLimit] [decimal](18, 4) NULL,
	[executorId] [uniqueidentifier] NULL,
	[arrayIndexColumn] [int] NULL,
	[version] [bigint] NOT NULL CONSTRAINT [DF_Processor_version]  DEFAULT ((0)),
 CONSTRAINT [PK_Processor] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JobExecutionMapping]    Script Date: 10/25/2009 00:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobExecutionMapping](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[created] [bigint] NOT NULL,
	[jobId] [uniqueidentifier] NOT NULL,
	[executorId] [uniqueidentifier] NOT NULL,
	[version] [bigint] NOT NULL,
 CONSTRAINT [PK_JobExecutionMapping] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Result]    Script Date: 10/25/2009 00:51:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Result](
	[id] [uniqueidentifier] NOT NULL,
	[jobId] [uniqueidentifier] NULL,
	[fileName] [varchar](1000) NOT NULL,
	[arrayIndexColumn] [bigint] NULL,
	[contentDataId] [bigint] NULL,
	[retrievalMode] [smallint] NOT NULL,
	[version] [bigint] NOT NULL CONSTRAINT [DF_Result_version]  DEFAULT ((0)),
 CONSTRAINT [PK_Result] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_Application_Dependency_ApplicationId]    Script Date: 10/25/2009 00:51:17 ******/
ALTER TABLE [dbo].[Application_Dependency]  WITH CHECK ADD  CONSTRAINT [FK_Application_Dependency_ApplicationId] FOREIGN KEY([applicationId])
REFERENCES [dbo].[Application] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Application_Dependency] CHECK CONSTRAINT [FK_Application_Dependency_ApplicationId]
GO
/****** Object:  ForeignKey [FK_Application_Dependency_DependencyId]    Script Date: 10/25/2009 00:51:17 ******/
ALTER TABLE [dbo].[Application_Dependency]  WITH CHECK ADD  CONSTRAINT [FK_Application_Dependency_DependencyId] FOREIGN KEY([dependencyId])
REFERENCES [dbo].[Dependency] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Application_Dependency] CHECK CONSTRAINT [FK_Application_Dependency_DependencyId]
GO
/****** Object:  ForeignKey [FK_Dependency_BinaryData]    Script Date: 10/25/2009 00:51:20 ******/
ALTER TABLE [dbo].[Dependency]  WITH CHECK ADD  CONSTRAINT [FK_Dependency_BinaryData] FOREIGN KEY([contentDataId])
REFERENCES [dbo].[BinaryData] ([id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Dependency] CHECK CONSTRAINT [FK_Dependency_BinaryData]
GO
/****** Object:  ForeignKey [FK_DiskVolume_Executor]    Script Date: 10/25/2009 00:51:22 ******/
ALTER TABLE [dbo].[DiskVolume]  WITH CHECK ADD  CONSTRAINT [FK_DiskVolume_Executor] FOREIGN KEY([executorId])
REFERENCES [dbo].[Executor] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DiskVolume] CHECK CONSTRAINT [FK_DiskVolume_Executor]
GO
/****** Object:  ForeignKey [FK_Grp_Permission_Grp]    Script Date: 10/25/2009 00:51:26 ******/
ALTER TABLE [dbo].[Grp_Permission]  WITH CHECK ADD  CONSTRAINT [FK_Grp_Permission_Grp] FOREIGN KEY([grpId])
REFERENCES [dbo].[Grp] ([grpId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Grp_Permission] CHECK CONSTRAINT [FK_Grp_Permission_Grp]
GO
/****** Object:  ForeignKey [FK_Grp_Permission_Permission]    Script Date: 10/25/2009 00:51:26 ******/
ALTER TABLE [dbo].[Grp_Permission]  WITH CHECK ADD  CONSTRAINT [FK_Grp_Permission_Permission] FOREIGN KEY([permissionId])
REFERENCES [dbo].[Permission] ([permissionId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Grp_Permission] CHECK CONSTRAINT [FK_Grp_Permission_Permission]
GO
/****** Object:  ForeignKey [FK_Job_Application]    Script Date: 10/25/2009 00:51:29 ******/
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_Application] FOREIGN KEY([applicationId])
REFERENCES [dbo].[Application] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_Application]
GO
/****** Object:  ForeignKey [FK_Job_BinaryData_InitialInstance]    Script Date: 10/25/2009 00:51:30 ******/
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_BinaryData_InitialInstance] FOREIGN KEY([initialInstanceDataId])
REFERENCES [dbo].[BinaryData] ([id])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_BinaryData_InitialInstance]
GO
/****** Object:  ForeignKey [FK_Job_BinaryData_Instance]    Script Date: 10/25/2009 00:51:30 ******/
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_BinaryData_Instance] FOREIGN KEY([instanceDataId])
REFERENCES [dbo].[BinaryData] ([id])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_BinaryData_Instance]
GO
/****** Object:  ForeignKey [FK_Job_BinaryData_ExceptionInstance]    Script Date: 03/21/2010 23:04:00 ******/
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_BinaryData_ExceptionInstance] FOREIGN KEY([exceptionInstanceDataId])
REFERENCES [dbo].[BinaryData] ([id])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_BinaryData_ExceptionInstance]
GO
/****** Object:  ForeignKey [FK_Job_Executor]    Script Date: 10/25/2009 00:51:30 ******/
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_Executor] FOREIGN KEY([executorId])
REFERENCES [dbo].[Executor] ([id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_Executor]
GO
/****** Object:  ForeignKey [FK_Job_Dependency_Dependency]    Script Date: 10/25/2009 00:51:31 ******/
ALTER TABLE [dbo].[Job_Dependency]  WITH CHECK ADD  CONSTRAINT [FK_Job_Dependency_Dependency] FOREIGN KEY([dependencyId])
REFERENCES [dbo].[Dependency] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Job_Dependency] CHECK CONSTRAINT [FK_Job_Dependency_Dependency]
GO
/****** Object:  ForeignKey [FK_Job_Dependency_Job]    Script Date: 10/25/2009 00:51:31 ******/
ALTER TABLE [dbo].[Job_Dependency]  WITH CHECK ADD  CONSTRAINT [FK_Job_Dependency_Job] FOREIGN KEY([jobId])
REFERENCES [dbo].[Job] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Job_Dependency] CHECK CONSTRAINT [FK_Job_Dependency_Job]
GO
/****** Object:  ForeignKey [FK_JobExecutionMapping_Executor]    Script Date: 10/25/2009 00:51:32 ******/
ALTER TABLE [dbo].[JobExecutionMapping]  WITH CHECK ADD  CONSTRAINT [FK_JobExecutionMapping_Executor] FOREIGN KEY([executorId])
REFERENCES [dbo].[Executor] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JobExecutionMapping] CHECK CONSTRAINT [FK_JobExecutionMapping_Executor]
GO
/****** Object:  ForeignKey [FK_JobExecutionMapping_Job]    Script Date: 10/25/2009 00:51:32 ******/
ALTER TABLE [dbo].[JobExecutionMapping]  WITH CHECK ADD  CONSTRAINT [FK_JobExecutionMapping_Job] FOREIGN KEY([jobId])
REFERENCES [dbo].[Job] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JobExecutionMapping] CHECK CONSTRAINT [FK_JobExecutionMapping_Job]
GO
/****** Object:  ForeignKey [FK_Memory_Executor]    Script Date: 10/25/2009 00:51:34 ******/
ALTER TABLE [dbo].[Memory]  WITH CHECK ADD  CONSTRAINT [FK_Memory_Executor] FOREIGN KEY([executorId])
REFERENCES [dbo].[Executor] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Memory] CHECK CONSTRAINT [FK_Memory_Executor]
GO
/****** Object:  ForeignKey [FK_Processor_Executor]    Script Date: 10/25/2009 00:51:38 ******/
ALTER TABLE [dbo].[Processor]  WITH CHECK ADD  CONSTRAINT [FK_Processor_Executor] FOREIGN KEY([executorId])
REFERENCES [dbo].[Executor] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Processor] CHECK CONSTRAINT [FK_Processor_Executor]
GO
/****** Object:  ForeignKey [FK_Result_Job]    Script Date: 10/25/2009 00:51:39 ******/
ALTER TABLE [dbo].[Result]  WITH CHECK ADD  CONSTRAINT [FK_Result_Job] FOREIGN KEY([jobId])
REFERENCES [dbo].[Job] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Result] CHECK CONSTRAINT [FK_Result_Job]
GO
/****** Object:  ForeignKey [FK_Usr_Grp]    Script Date: 10/25/2009 00:51:41 ******/
ALTER TABLE [dbo].[Usr]  WITH CHECK ADD  CONSTRAINT [FK_Usr_Grp] FOREIGN KEY([grpId])
REFERENCES [dbo].[Grp] ([grpId])
GO
ALTER TABLE [dbo].[Usr] CHECK CONSTRAINT [FK_Usr_Grp]
GO
