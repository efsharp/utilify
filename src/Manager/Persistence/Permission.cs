﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilify.Platform.Manager.Persistence
{
    [Serializable]
    internal class Permission
    {
        private int id;
        private string name; 
        private string description;

        public int Id //todoFix finish validation etc. for the setter
        {
            get
            {
                return id;
            }
            private set
            {
                this.id = value; 
            }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

    }
}
