using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;

namespace Utilify.Platform
{
    /// <summary>
    /// Helper class for resolving assembly references and dependencies.
    /// This class is for internal use only.
    /// </summary>
    public static class DependencyResolver
    {
        //todo: when creating the installer, we need to check this known assemblies once
        private const string PlatFormAssembliesResource = "Utilify.Framework.API.KnownAssemblies.xml";
        private static Logger logger = new Logger();

        //cache of already loaded assemblies
        // Key (string) = assemblyFullName, Value (Assembly) = Assembly object that corresponds to the full name
        private static Dictionary<string, Assembly> assembliesLoaded = new Dictionary<string, Assembly>();

        //cache of types already seen
        private static Dictionary<Type, List<DependencyInfo>> typesSeen = new Dictionary<Type, List<DependencyInfo>>();

        //to make the GetPlatformAssemblies thread-safe.
        private static object platformAsmSyncObject = new object();

        //The loading of platformAssemblies from embedded xml may cause some perf hit. Possible options:
        //1. test to see if embedded txt resource improves speed
        //2. test to see if embedded string list improves speed 
        //(both #1, and #2 however may limit flexibility - need to test and see how much impact this method really has)
        private static List<string> platformAssemblies;

        private static List<string> GetPlatformAssemblies()
        {
            if (platformAssemblies == null)
            {
                lock (platformAsmSyncObject)
                {
                    platformAssemblies = new List<string>();
                    try
                    {
                        Assembly frameworkAsm = Assembly.GetAssembly(typeof(DependencyResolver));
                        XmlDocument xml = new XmlDocument();
                        using (Stream s = frameworkAsm.GetManifestResourceStream(PlatFormAssembliesResource))
                        {
                            xml.Load(s);
                        }
                        XmlNodeList nodeList = xml.SelectNodes("assemblies/assembly");
                        if (nodeList != null)
                        {
                            foreach (XmlNode node in nodeList)
                            {
                                if (node != null)
                                    platformAssemblies.Add(node.Attributes["name"].Value);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Warn("Could not load list of platform assemblies from embedded resources. Using an empty list...", ex);
                    }
                }
            }
            return platformAssemblies;
        }

        //todo: need to test performance of this method: using reflection may slow it down
        //other alternatives: introspection. 
        //1. Mono.Cecil (is it quick enough?)
        //2. FxCop (can't be re-dist, perhaps contact MS?) (does it work on Mono?) 
        //3. Lutz Reflector API (don't know if this can be re-dist / does it work on Mono?)
        /// <summary>
        /// Detects the dependencies for the given type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>List of dependencies</returns>
        public static List<DependencyInfo> DetectDependencies(Type type)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            //try looking in the cache first - before doing any work:
            lock (platformAsmSyncObject) //use the same lock object for the assemblies list
            {
                if (typesSeen.ContainsKey(type))
                    return typesSeen[type];
            }

            //todoPerf: move this into a class-level static dictionary, and look up when needed with locking.
            // Key (string) = assemblyFullName, Value (DependencyInfo) = DependencyInfo object for the assembly file
            Dictionary<string, DependencyInfo> dependencies = new Dictionary<string, DependencyInfo>();

            List<string> stdAssemblies = null;
            lock (platformAsmSyncObject)
            {
                //it should be ok to have the same reference, 
                //since we only read this list - and don't modify it
                stdAssemblies = GetPlatformAssemblies(); 

                //add the current framework assembly: this way we always compare against current framework version
                //and the Platform.Shared assembly
                stdAssemblies.Add(typeof(IApplicationManager).Assembly.FullName);
                stdAssemblies.Add(typeof(DependencyResolver).Assembly.FullName);
            }

            //list of asssembly full names that need to be checked
            Stack<string> assembliesToCheck = new Stack<string>();
            assembliesToCheck.Push(type.Assembly.FullName);
            
            while (assembliesToCheck.Count > 0)
            {
                string currentAssemblyFullName = assembliesToCheck.Pop();

                //double check to make sure we don't process the same assembly twice
                if (dependencies.ContainsKey(currentAssemblyFullName))
                    continue;

                //check if it is a standard assembly
                if (stdAssemblies.Contains(currentAssemblyFullName))
                    continue;

                //load assembly (either from local cache / using a reflection-only context)
                Assembly currentAssembly = LoadAssembly(currentAssemblyFullName);

                logger.Debug("Adding dependency for current assembly: " + currentAssemblyFullName);

                //add the dependency for the current assembly to list of detected dependencies
                DependencyInfo dep = CreateDependencyFromAssembly(currentAssembly);
                dependencies[currentAssemblyFullName] = dep;

                logger.Debug("Getting references for current assembly: " + currentAssemblyFullName);
                AssemblyName[] references = currentAssembly.GetReferencedAssemblies(); //we know this shouldn't return null, so skip check
                foreach (AssemblyName asm in references)
                {
                    string referenceFullName = asm.FullName; //we know asm shouldnt be null, so skip check
                    if (!dependencies.ContainsKey(referenceFullName) &&
                        !stdAssemblies.Contains(referenceFullName) &&
                        !assembliesToCheck.Contains(referenceFullName))
                    {
                        //push it onto the stack for processing in the next round
                        assembliesToCheck.Push(referenceFullName);
                    }
                }
            }

            List<DependencyInfo> detected = new List<DependencyInfo>();
            detected.AddRange(dependencies.Values);

            //finally put it into our cache of types - so we don't ever have to do this again for the same type:
            lock (platformAsmSyncObject) //use the same lock object for the assemblies list
            {
                if (!typesSeen.ContainsKey(type))
                    typesSeen[type] = detected;
            }
            return detected;
        }

        private static Assembly LoadAssembly(string assemblyFullName)
        {
            Assembly assembly = null;
            lock (assembliesLoaded)
            {
                //get it from our local cache if it exists
                if (assembliesLoaded.ContainsKey(assemblyFullName))
                    assembly = assembliesLoaded[assemblyFullName];
                else //load the assembly using reflection only context
                {
                    assembly = Assembly.ReflectionOnlyLoad(assemblyFullName);
                    assembliesLoaded[assemblyFullName] = assembly;
                }
            }
            return assembly;
        }

        //we shouldnt save the assembly.Location on the Manager.
        private static DependencyInfo CreateDependencyFromAssembly(Assembly assembly)
        {
            FileInfo file = new FileInfo(assembly.Location);

            //give it the full name here : but it wont be sent out to the manager : since we take only the filename
            //in the DependencyInfo ctors
            DependencyInfo dep = new DependencyInfo(file.Name, file.FullName, DependencyType.ClrModule);

            return dep;
        }
    }
}
