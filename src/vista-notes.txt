x64:

- Compiled using Platform AnyCPU. (need to look into the differences between x64 and 'Any CPU')
- Works well.

- Best to have a seperate service account set up at installation time.
- We need the Executor Service account to be part of the "Performance Monitor Users" group.

- The Manager service account needs to be given read permissions to the private key of the server certificate used. This somehow 
will need to be done at installation time too.