﻿namespace Utilify.Platform.Configuration
{
    /// <summary>
    /// Used for additional validation on top of the validation done by the .NET configuration system
    /// </summary>
    public interface IConfigurationElement
    {
        /// <summary>
        /// Validates this instance.
        /// </summary>
        void Validate();
    } //todo: do we rename this IValidatable?
}