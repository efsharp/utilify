using System;
using System.Configuration;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Utilify.Platform.Configuration;

#if !MONO
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using Utilify.ServiceModel;
#endif

namespace Utilify.Platform
{
    /// <summary>
    /// Helper class to create service proxy instances.
    /// This class is for internal use only.
    /// </summary>
    public static class ProxyFactory
    {
        //todoFix: make the ProxyFactory a non-static class: so that we can make it more generic - 
        //and use it for services that don't have this service path in the url 
        //i.e for services where we need a different path component of the url, but still need our custom binding tweaks

        /// <summary>
        /// Specifies the common service path for services.
        /// </summary>
        public const string CommonServicePath = "Utilify.Platform.Manager"; 

        //todo: need to seperate internal logging and public logging.
        //Logger can then be made internal - and we expose the logs through the public API - 
        //eg: have a LogEvent in classes such as Application, ManagerHost, ExecutorHost etc.
        private static Logger logger = new Logger();

        /// <summary>
        /// Gets the remote service proxy for the given type T.
        /// The credentials and service host/url information is pulled from the Client configuration section
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>An instance of the proxy used to communicate with the remote service</returns>
        public static T CreateProxyInstance<T>() where T : class, IManagerService
        {
            return DoCreateProxy<T>(null);
        }

        /// <summary>
        /// Gets the remote service proxy for the given type T.
        /// The credentials and service host/url information is pulled from the Client configuration section
        /// if the given settings is null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="settings">The connection settings used to connect to the manager</param>
        /// <returns>An instance of the proxy used to communicate with the remote service</returns>
        public static T CreateProxyInstance<T>(ConnectionSettings settings) where T : class, IManagerService
        {
            return DoCreateProxy<T>(settings);
        }

        private static T DoCreateProxy<T>(ConnectionSettings settings) where T : class, IManagerService
        {
            //maybe this is not such a good idea to use generics here.
            //since we cant put proper constraints at compile time
            //but we can get nice syntax for the user of this method

            ServiceType type = GetServiceType<T>();

            if (settings == null || settings.IsDefault)
                settings = ConfigurationHelper.GetConnectionSettings(type, ConfigurationUserLevel.None, false);
                
            object proxy = null;

#if MONO
            //use remoting to connect from a Linux/Mono machine to the manager:
            proxy = RemotingHelper.GetObject<T>();
#else
            Type proxyType = null;
            string proxyTypeName = null;
            if (!TryGetProxyType<T>(out proxyType, out proxyTypeName))
                throw new ArgumentException("Cannot create proxy for unknown type : " + typeof(T).FullName + ", Could not find proxy type: " + proxyTypeName);

            //use WCF to connect from a Windows+.NET machine to the manager:
            //Windows+Mono is not a supported configuration

            //read binding related details from config and build binding, end-point for the given service type T.
            Binding binding = CreateBinding<T>(settings);
            EndpointAddress endPoint = GetEndPointAddress<T>(settings);

            proxy = CreateWcfProxy(proxyType, binding, endPoint);

            //if we need to provide any credentials, pull that from config as well
            IManagerProxy wcfProxy = (IManagerProxy)proxy;

            SetCredentials(settings, ref wcfProxy);
#endif
            return (T)proxy;
        }

        private static ServiceType GetServiceType<T>() where T : class, IManagerService
        {
            ServiceType type = ServiceType.ApplicationManager; //default
            string typeName = GetServiceName<T>();
            if (Enum.IsDefined(typeof(ServiceType), typeName))
            {
                type = (ServiceType)Enum.Parse(typeof(ServiceType), typeName);
            }
            return type;
        }

        private static bool TryGetProxyType<T>(out Type proxyType, out string proxyTypeName) where T : class, IManagerService
        {
            //this is a bit of a hack : but it saves us from knowing beforehand the types 
            //that we allow - as long as this convention is followed:
            //the convention here is to have a Proxy class that extends from WCFProxyBase<T>
            //and is named after the interface as:
            // XyzProxy, where Xyz = name of the interface without the 'I'.

            proxyType = null;
            proxyTypeName = null;

            Type interfaceType = typeof(T);

            //enforce naming convention
            if (interfaceType.Name.StartsWith("I"))
            {
                string proxyTypeNameTemp = null;
                //get the name of the proxy without the 'I' and add a 'Proxy' to it!
                proxyTypeNameTemp = string.Format("{0}.{1}Proxy", interfaceType.Namespace, interfaceType.Name.Substring(1));
                
                //1. first try entry assembly. (in our code, this is the most likely place where the proxy class is.)
                Assembly entryAsm = Assembly.GetEntryAssembly();
                if (entryAsm != null) //play safe: it can be null in some situations like nunit tests
                {
                    proxyTypeName = Assembly.CreateQualifiedName(entryAsm.FullName,
                        proxyTypeNameTemp);

                    //logger.Debug("1. Trying to get type from : " + proxyTypeName);
                    //      try to get the type, but don't throw an exception if it is not found.
                    proxyType = Type.GetType(proxyTypeName, false, false);
                }

                //2. next try in the current assembly, if it is different from the entry assembly
                Assembly executingAsm = Assembly.GetExecutingAssembly();
                if (proxyType == null && executingAsm != null)
                {
                    if (entryAsm != executingAsm)
                    {
                        proxyTypeName = Assembly.CreateQualifiedName(executingAsm.FullName,
                            proxyTypeNameTemp);

                        //logger.Debug("2. Trying to get type from : " + proxyTypeName);
                        //      try to get the type, but don't throw an exception if it is not found.
                        proxyType = Type.GetType(proxyTypeName, false, false);
                    }
                }

                //3. finally, try in the assembly where the interface is defined - if it is different from the 
                //executing assembly and entry assembly.
                if (proxyType == null)
                {
                    Assembly interfaceAsm = interfaceType.Assembly;
                    if (interfaceAsm != entryAsm && interfaceAsm != executingAsm)
                    {
                        proxyTypeName = Assembly.CreateQualifiedName(interfaceAsm.FullName,
                            proxyTypeNameTemp);

                        //logger.Debug("3. Trying to get type from : " + proxyTypeName);

                        //      try to get the type, but don't throw an exception if it is not found.
                        proxyType = Type.GetType(proxyTypeName, false, false);
                    }
                }
            }

            return (proxyType != null);
        }

        internal static string GetServiceName<T>() where T : class, IManagerService
        {
            return GetServiceName(typeof(T));
        }

        //26-sep-09: for internal use only: had to make it public to avoid InternalsVisibleTo:
        //method used in the Manager when creating the services to host
        /// <summary>
        /// Gets the relative address for a service using the specified credential type.
        /// The full service path is generally the base path followed by the credential type.
        /// </summary>
        /// <param name="cred">The credential type used to authenticate client requests.</param>
        /// <returns></returns>
        public static string GetRelativeAddress(CredentialType cred)
        {
            if (cred != CredentialType.None)
                return string.Format("{0}Auth", cred.ToString());
            else
                return string.Empty;
        }

        //26-sep-09: don't need this anymore: using simpler: IDisposable.Dispose
//        internal static void CloseProxy<T>(T proxy) where T : class, IManagerService
//        {
//#if !MONO
//            IWCFProxy wcfProxy = proxy as IWCFProxy;

//            if (wcfProxy == null)
//                return; //we dispose only Wcf proxies - not any random class
 
//            wcfProxy.Close();
//#endif
//        }

#if !MONO
        private static void SetCredentials(ConnectionSettings settings, ref IManagerProxy wcfProxy)
        {
            if (wcfProxy == null)
                return;

            //no security mode specified
            if (settings != null && settings.CredentialType == CredentialType.None)
                return;

            //we know that if we get here, we have a valid section
            switch (settings.CredentialType)
            {
                case CredentialType.Windows:
                    break; //nothing to do: WCF will automatically pick up the credentials for the user running this process.
                case CredentialType.Username:

                    UsernameCredential utilifyCred = settings.UsernameCredential;

                    UserNamePasswordClientCredential cred = wcfProxy.ClientCredentials.UserName;
                    cred.UserName = utilifyCred.Username;
                    cred.Password = utilifyCred.Password;

                    break;
            }

            X509ServiceCertificateAuthentication serverAuth = wcfProxy.ClientCredentials.ServiceCertificate.Authentication;
            if (settings.ServiceCertificate.Validate)
            {
                serverAuth.CertificateValidationMode = X509CertificateValidationMode.PeerOrChainTrust;
                serverAuth.TrustedStoreLocation = StoreLocation.LocalMachine;
            }
            else
            {
                serverAuth.CertificateValidationMode = X509CertificateValidationMode.None;
            }

            //#if DEBUG
            //            if (wcfProxy.ClientCredentials.Windows != null)
            //            {
            //                logger.Debug("wcfProxy AllowedImpersonationLevel = " + wcfProxy.ClientCredentials.Windows.AllowedImpersonationLevel);
            //                logger.Debug("AllowNtlm = " + wcfProxy.ClientCredentials.Windows.AllowNtlm);
            //            }
            //#endif

        }

        private static object CreateWcfProxy(Type proxyType, Binding binding, EndpointAddress endPoint)
        {
            //we use reflection to create the instance and hand it back to the caller:
            //we look for the classes in the same namespace as the interface itself.
            //the proxy class needs to have a constructor that takes in a Binding and EndpointAddress.

            object proxy = null;

            proxy = Activator.CreateInstance(proxyType, new object[] { binding, endPoint });

            return proxy;
        }

        //todo: could use a TypeExtensions class and put this method there as an extension method on the 'Type' type.
        private static bool IsStreamableContract(Type contractType)
        {
            bool isStreamed = false;
            object[] attrs = contractType.GetCustomAttributes(typeof(StreamedBehaviorAttribute), false);
            if (attrs != null && attrs.Length == 1)
            {
                isStreamed = true;
            }
            return isStreamed;
        }

        private static Binding DoCreateBinding(Type contractType, bool isInteropClient, CredentialType credentialType)
        {
            Binding binding = null;

            //todo: check these settings for both bindings
            //for username creds, we need a service certificate.
            //And if provided, we may need to change the DnsIdentity value for the service cert.

            bool isStreamed = IsStreamableContract(contractType);
            //todo: need to reset the security mode here? what do we set it to?
            //todo: for streamed mode, do we need to reset the security mode here? what do we set it to?
            //does streaming work with all security modes?

            if (!isInteropClient)
            {
                NetTcpBinding netTcp = new NetTcpBinding();
                if (credentialType == CredentialType.Windows)
                {
                    netTcp.Security.Mode = SecurityMode.Transport;
                    netTcp.Security.Transport.ClientCredentialType = TcpClientCredentialType.Windows;
                }
                else if (credentialType == CredentialType.Username)
                {
                    netTcp.Security.Mode = SecurityMode.TransportWithMessageCredential;
                    netTcp.Security.Message.ClientCredentialType = MessageCredentialType.UserName;
                }
                else if (credentialType == CredentialType.None)
                {
                    netTcp.Security.Mode = SecurityMode.None;
                }

                if (credentialType != CredentialType.None)
                    netTcp.Security.Transport.ProtectionLevel = ProtectionLevel.EncryptAndSign;

                netTcp.MaxReceivedMessageSize = 64 * Constants.Mega; //MAGIC
                netTcp.ReaderQuotas = CreateReaderQuotas();

                if (isStreamed) //this probably won't work with Message security
                    netTcp.TransferMode = TransferMode.Streamed;

                binding = CreateCustomBinding(netTcp, credentialType);
                //binding = netTcp;

            }
            else
            {
                //todo: check this: streaming cannot be enabled for wsHttp bindings:

                WSHttpBinding wsHttp = new WSHttpBinding();
                if (credentialType != CredentialType.None)
                    wsHttp.Security.Mode = SecurityMode.Message; //this way we can support different creds + use http

                if (credentialType == CredentialType.Windows)
                {
                    wsHttp.Security.Message.ClientCredentialType = MessageCredentialType.Windows;
                }
                else if (credentialType == CredentialType.Username)
                {
                    wsHttp.Security.Message.ClientCredentialType = MessageCredentialType.UserName;
                }
                else if (credentialType == CredentialType.None)
                {
                    wsHttp.Security.Mode = SecurityMode.None;
                }

                wsHttp.Security.Message.EstablishSecurityContext = true;
                wsHttp.Security.Message.NegotiateServiceCredential = true;
                wsHttp.MaxReceivedMessageSize = 64 * Constants.Mega;
                wsHttp.ReaderQuotas = CreateReaderQuotas();

                wsHttp.HostNameComparisonMode = HostNameComparisonMode.Exact; //for Azure

                binding = wsHttp;

                //case CommunicationProtocol.BasicHttp:

                //    BasicHttpBinding basicHttp = new BasicHttpBinding();

                //    if (credentialType != CredentialType.None)
                //        basicHttp.Security.Mode = BasicHttpSecurityMode.None;
                //    else
                //        basicHttp.Security.Mode = BasicHttpSecurityMode.TransportWithMessageCredential;

                //    if (credentialType == CredentialType.Username)
                //    {
                //        basicHttp.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
                //    }
                //    //else if (credentialType == CredentialType.Certificate)
                //    //{
                //    //    basicHttp.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.Certificate;
                //    //}

                //    basicHttp.ReaderQuotas = CreateReaderQuotas();
                //    binding = basicHttp;

                //    break;
            }

            //if we still can't create a binding
            if (binding == null)
                throw new ConfigurationErrorsException("No suitable protocol was configured to connect to the server.");

            //finally, set some binding timeouts : more MAGIC
            if (isStreamed)
            {
                //6 hrs!!!
                binding.SendTimeout = new TimeSpan(6, 0, 0);
                binding.ReceiveTimeout = new TimeSpan(6, 0, 0);
            }
            else
            {
                //1 hr!!!
                binding.SendTimeout = new TimeSpan(1, 0, 0);
                binding.ReceiveTimeout = new TimeSpan(1, 0, 0);
            }
            return binding;
        }

        //todo: should we move this into its own class?
        /// <summary>
        /// Creates a binding given the contract type
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="isInteropClient"></param>
        /// <param name="credentialType"></param>
        /// <returns></returns>
        public static Binding CreateBinding(Type contract, bool isInteropClient, CredentialType credentialType)
        {
           return DoCreateBinding(contract, isInteropClient, credentialType);
        }

        /// <summary>
        /// Creates a WCF binding from the settings in the Client configuration section.
        /// </summary>
        /// <typeparam name="T">The type of the contract interface</typeparam>
        /// <returns></returns>
        private static Binding CreateBinding<T>(ConnectionSettings settings) where T : class, IManagerService
        {
            return DoCreateBinding(typeof(T), settings.IsInteropClient, settings.CredentialType);
        }

        private static Binding CreateCustomBinding(NetTcpBinding bindingToCopy, CredentialType credentialType)
        {
            var cb = new CustomBinding();

            cb.Name = bindingToCopy.Name;
            cb.Namespace = bindingToCopy.Namespace;
            cb.CloseTimeout = bindingToCopy.CloseTimeout;
            cb.OpenTimeout = bindingToCopy.OpenTimeout;
            cb.ReceiveTimeout = bindingToCopy.ReceiveTimeout;
            cb.SendTimeout = bindingToCopy.SendTimeout;

            var elements = bindingToCopy.CreateBindingElements();

            //ignoring transaction flow and reliable session binding elements
            
            //1: message security element - if needed
            if (credentialType == CredentialType.Username)
            {
                var usernameOverTransport = SecurityBindingElement.CreateUserNameOverTransportBindingElement();
                usernameOverTransport.DefaultAlgorithmSuite = bindingToCopy.Security.Message.AlgorithmSuite;
                usernameOverTransport.MessageSecurityVersion = MessageSecurityVersion.WSSecurity11WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11;

                var msgSecurityElement = SecurityBindingElement.CreateSecureConversationBindingElement(usernameOverTransport);
                msgSecurityElement.DefaultAlgorithmSuite = bindingToCopy.Security.Message.AlgorithmSuite;
                msgSecurityElement.IncludeTimestamp = true;
                msgSecurityElement.LocalClientSettings.ReconnectTransportOnFailure = false;
                msgSecurityElement.LocalServiceSettings.ReconnectTransportOnFailure = false;
                msgSecurityElement.MessageSecurityVersion = MessageSecurityVersion.WSSecurity11WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11;
                
                cb.Elements.Add(msgSecurityElement);
            }

            //2: encoding element
            var encoder = elements.Remove<BinaryMessageEncodingBindingElement>();
            cb.Elements.Add(new GZipMessageEncodingBindingElement(encoder));
            
            //3: transport security element
            BindingElement transportSecurityElement = null;
            if (credentialType == CredentialType.Username)
            {
                //we have NetTcp with TransportWithMessageCredential security
 
                var se = new SslStreamSecurityBindingElement();
                se.RequireClientCertificate = false;
                
                transportSecurityElement = se;
            }
            else if (credentialType == CredentialType.Windows)
            {
                //NetTcp with Transport security

                var se = new WindowsStreamSecurityBindingElement();
                se.ProtectionLevel = ProtectionLevel.EncryptAndSign;

                transportSecurityElement = se;
            }
            cb.Elements.Add(transportSecurityElement);

            //4: transport element
            var transportElement = elements.Remove<TcpTransportBindingElement>();
            cb.Elements.Add(transportElement);

            return cb;
        }

        /// <summary>
        /// Creates the XML reader quotas for services and clients.
        /// This method is for internal use only.
        /// </summary>
        /// <returns></returns>
        private static System.Xml.XmlDictionaryReaderQuotas CreateReaderQuotas()
        {
            //more MAGIC numbers!
            System.Xml.XmlDictionaryReaderQuotas readerQuotas = new System.Xml.XmlDictionaryReaderQuotas();

            readerQuotas.MaxDepth = 8 * Constants.Mega;
            readerQuotas.MaxStringContentLength = Constants.Giga;
            readerQuotas.MaxArrayLength = Constants.Giga;
            readerQuotas.MaxBytesPerRead = 128 * Constants.Kilo;
            readerQuotas.MaxNameTableCharCount = 128 * Constants.Kilo;

            return readerQuotas;
        }

        /// <summary>
        /// Gets the address of the endpoint for the specified type T, using the host and port settings from
        /// the Client configuration section.
        /// This method is for internal use only.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private static EndpointAddress GetEndPointAddress<T>(ConnectionSettings settings) where T : class, IManagerService
        {

            string proto = null;

            //it should always be one of these two, or else we'll get an exception during parsing the config file.
            if (settings.IsInteropClient)
            {
                proto = Uri.UriSchemeHttp;
                //we use message security: not transport, so we need http, not https.
                //if (settings.CredentialType == CredentialType.None)
                //{
                //    proto = Uri.UriSchemeHttp;
                //}
                //else
                //{
                //    proto = Uri.UriSchemeHttps;
                //}
            }
            else
            {
                proto = Uri.UriSchemeNetTcp;
            }

            string serviceName = GetServiceName<T>();

            int port = (settings.Port > 0) ? settings.Port : ClientElement.DefaultPort;

            string host = !string.IsNullOrEmpty(settings.Host) ?
                settings.Host : ClientElement.DefaultHost;

            CredentialType credType = settings.CredentialType;

            //for each cred-type the service uri is different
            UriBuilder ub = new UriBuilder(proto, host, port, 
                string.Concat(CommonServicePath, "/", serviceName, "/", GetRelativeAddress(credType)));

            DnsEndpointIdentity serviceIdentity = null;

            //check the credential we are using
            if (credType == CredentialType.Username) // || credType == CredentialType.Certificate)
            {
                //host is never meant to be null/empty
                if (!host.Equals(settings.ServiceCertificate.DnsIdentity))
                {
                    serviceIdentity = new DnsEndpointIdentity(settings.ServiceCertificate.DnsIdentity);
                }
            }

            EndpointAddress ep = null;
            if (serviceIdentity != null)
                ep = new EndpointAddress(ub.Uri, serviceIdentity);
            else
                ep = new EndpointAddress(ub.Uri);

            return ep;
        }

#endif

        internal static string GetServiceName(Type contractType)
        {
            //this is a bit of a hack, but it saves us from hard-coding names,
            //and also from having to reference the interfaces from the Runtime dll in the framework.

            //type would typically be an interface such as IApplicationManager, IWorkloadManager etc.
            //the convention here, is to have a service name that is the same as the interface name without the 'I'
            string serviceName = contractType.Name;
            if (serviceName.StartsWith("I"))
                serviceName = serviceName.Substring(1); //everything after the 'I'

            return serviceName;
        }
    }
}
