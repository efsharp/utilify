﻿using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Security
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class UserInfo
    {
        private string name;
        private string password;
        private string windowsUsername;
        private string certificateName;
        private GroupInfo group;

        public UserInfo() { }

        public UserInfo(string name, string password, GroupInfo group)
            : this(name, password, null, null, group) { }

        public UserInfo(string name, string password, string windowsUsername, string certificateName, GroupInfo group) : this()
        {
            this.name = name;
            this.password = password;
            this.windowsUsername = windowsUsername;
            this.certificateName = certificateName;
            this.group = group;
        }

#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

#if !MONO
        [DataMember(IsRequired = false)]
#endif
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

#if !MONO
        [DataMember(IsRequired = false)]
#endif
        public string WindowsUsername
        {
            get { return windowsUsername; }
            set { windowsUsername = value; }
        }

#if !MONO
        [DataMember(IsRequired = false)]
#endif
        public string CertificateName
        {
            get { return certificateName; }
            set { certificateName = value; }
        }

#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public GroupInfo Group
        {
            get { return group; }
            set { group = value; }
        }
    }
}
