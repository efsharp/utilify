﻿using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Security
{

    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class PermissionInfo
    {
        private int id;
        private string name; 
        private string description;

#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                if (id <= 0)
                    throw new ArgumentOutOfRangeException("id");

                this.id = value;
            }
        }

#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

    }
}
