using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class ProcessorPerformanceInfo
    {
        private string name; // OS's name/id for this CPU used for linking during the updates
        private double speedCurrent; //(in Ghz)
        private double loadTotalCurrent; //(in %)
        private double loadUsageCurrent; //(in %)

        private ProcessorPerformanceInfo(){}

        /// <summary>
        /// Creates an instance of ProcessorPerformanceInfo.
        /// </summary>
        /// <param name="name">the name of this processor</param>
        /// <param name="speedCurrent">the current clock speed (in MHz) for this processor</param>
        /// <param name="loadTotalCurrent">the total load for this processor (%)</param>
        /// <param name="loadUsageCurrent">the cpu usage for the executor process itself (%)</param>
        public ProcessorPerformanceInfo(string name, double speedCurrent, double loadTotalCurrent, double loadUsageCurrent)
            : this()
        {
            this.Name = name;
            this.SpeedCurrent = speedCurrent;
            this.LoadTotalCurrent = loadTotalCurrent;
            this.LoadUsageCurrent = loadUsageCurrent;
        }

        /// <summary>
        /// Gets the Name of this processor
        /// </summary>
        /// <remarks>
        /// This field uniquely identifies each processor on a machine.
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Name
        {
            get { return name; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("name");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Processor name cannot be empty", "name");
                name = value;
            }
        }

        /// <summary>
        /// Gets the current clock speed (in MHz) for this processor.
        /// </summary>
        /// <remarks>
        /// Current clock speed may be lowered by a machine in order to reduce power consumption etc.
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public double SpeedCurrent
        {
            get { return speedCurrent; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Current processor speed cannot be less than zero", "speedCurrent");
                speedCurrent = value;
            }
        }

        /// <summary>
        /// Gets the total current load for this processor (%).
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public double LoadTotalCurrent
        {
            get { return loadTotalCurrent; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Current total processor load cannot be less than zero", "loadTotalCurrent");
                loadTotalCurrent = value;
            }
        }

        /// <summary>
        /// Gets the current load used by only the executor itself (%)
        /// </summary>
        /// <remarks>
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public double LoadUsageCurrent
        {
            get { return loadUsageCurrent; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Current processor load usage cannot be less than zero", "loadUsageCurrent");
                loadUsageCurrent = value;
            }
        }

        /// <summary>
        /// Gets the string representation of this object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("CPU {0} : {1} GHz Current Speed : {2}% Current Load Total {3}% Load Usage Current", 
                name, speedCurrent, loadTotalCurrent, loadUsageCurrent);
        }
    }
}
