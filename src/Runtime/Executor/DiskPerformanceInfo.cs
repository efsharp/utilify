using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class DiskPerformanceInfo
    {
        private string root; // OS's name/id for the root used for linking by performance and limit infos
        private long sizeTotalFree; //(in bytes)
        private long sizeUsageCurrent; //(in bytes)

        private DiskPerformanceInfo(){}

        /// <summary>
        /// Creates an instance of DiskPerformanceInfo
        /// </summary>
        /// <param name="root"></param>
        /// <param name="sizeTotalFree"></param>
        /// <param name="sizeUsageCurrent"></param>
        public DiskPerformanceInfo(string root, long sizeTotalFree, long sizeUsageCurrent) : this()
        {
            this.Root = root;
            this.SizeTotalFree = sizeTotalFree;
            this.SizeUsageCurrent = sizeUsageCurrent;
        }

        /// <summary>
        /// Gets the Root for this disk.
        /// </summary>
        /// <remarks>
        /// This field uniquely identifies each disk on a machine.
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Root
        {
            get { return root; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("root");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Disk root cannot be empty", "root");
                root = value;
            }
        }

        /// <summary>
        /// Gets the total size (in bytes) of the space free on this disk.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public long SizeTotalFree
        {
            get { return sizeTotalFree; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Size cannot be less than zero", "sizeTotalFree");
                sizeTotalFree = value;
            }
        }

        /// <summary>
        /// Gets the size (in bytes) of the space currently used on this disk.
        /// </summary>
        /// <remarks>
        /// Could be used to represent (Total Size - Total Size Free) 
        /// OR could be used to represent the number of bytes actually used ONLY by the executor itself.
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public long SizeUsageCurrent
        {
            get { return sizeUsageCurrent; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Size cannot be less than zero", "sizeUsageCurrent");
                sizeUsageCurrent = value;
            }
        }

        /// <summary>
        /// Gets the string representation for this object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} : {1} bytes Total Free. Currently Using {2} bytes", root, sizeTotalFree, sizeUsageCurrent);
        }
    }
}
