using System;
using System.Text;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class PerformanceInfo
    {
        private string systemId;
        private DateTime lastPingTime;
        private ProcessorPerformanceInfo[] processors;
        private DiskPerformanceInfo[] disks;
        private MemoryPerformanceInfo memory;

        private PerformanceInfo() { }

        /// <summary>
        /// Creates an instance of the PerformanceInfo class.
        /// </summary>
        /// <param name="systemId"></param>
        /// <param name="processors"></param>
        /// <param name="disks"></param>
        /// <param name="memory"></param>
        public PerformanceInfo(string systemId, ProcessorPerformanceInfo[] processors, 
            DiskPerformanceInfo[] disks, MemoryPerformanceInfo memory) : this()
        {
            this.SystemId = systemId;
            this.Processors = processors;
            this.Disks = disks;
            this.Memory = memory;
        }

        /// <summary>
        /// Gets the Id of the executor this performance info belongs to
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string SystemId
        {
            get { return systemId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("systemId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("SystemId cannot be empty", "systemId");
                systemId = value;
            }
        }

        /// <summary>
        /// Gets the date when this Executor last contacted the manager.
        /// </summary>
        /// <remarks>
        /// Validates the ping time to make sure its a valid date and not a time in the future.
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime LastPingTime
        {
            get { return lastPingTime; }
            set //was internal. had to change it to avoid InternalsVisibleTo attribs.
            {
                DateTime temp = Helper.MakeUtc(value);
                if (temp > DateTime.UtcNow.AddMinutes(5)) //MAGIC - for some leniency in time-differences
                    throw new ArgumentOutOfRangeException("lastPingTime",
                        "Last ping time cannot be set to a time in the future. Current time UTC: " + DateTime.UtcNow + ", trying to set to: " + temp);
                else if (temp > DateTime.UtcNow)
                    temp = DateTime.UtcNow; //reset it to current time
                lastPingTime = temp;
            }
        }

        /// <summary>
        /// Gets the processor performance information of the system
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public ProcessorPerformanceInfo[] Processors
        {
            get { return processors; }
            private set
            {
                if (value == null)
                    processors = new ProcessorPerformanceInfo[0];
                else
                    processors = value;
            }
        }

        /// <summary>
        /// Gets the hard disk performance information of the system
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DiskPerformanceInfo[] Disks
        {
            get { return disks; }
            private set
            {
                if (value == null)
                    disks = new DiskPerformanceInfo[0];
                else
                    disks = value;
            }
        }

        /// <summary>
        /// Gets the memory performance information of the system
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public MemoryPerformanceInfo Memory
        {
            get { return memory; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("memory");
                memory = value;
            }
        }

        /// <summary>
        /// Get the string representation of this object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("PerformanceInfo : {0} ", systemId).AppendLine();

            if (processors != null)
            {
                sb.AppendFormat("- Processors : {0}", processors.Length).AppendLine();
                for (int i = 0; i < processors.Length; i++)
                    sb.AppendFormat("- {0} : {1}", i, processors[i]).AppendLine();
            }
            else
            {
                sb.AppendLine("- No processors.");
            }

            if (disks != null)
            {
                sb.AppendFormat("- Disks : {0}", disks.Length).AppendLine();
                for (int i = 0; i < disks.Length; i++)
                    sb.AppendFormat("- {0} : {1}", i, disks[i]).AppendLine();
            }
            else
            {
                sb.AppendLine("- No disks.");
            }

            sb.AppendFormat("- Memory : {0}", memory).AppendLine();

            return sb.ToString();
        }
    }
}
