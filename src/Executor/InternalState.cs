﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Utilify.Platform.Executor
{
    internal static class InternalState
    {
        private static object syncLock = new object();

        private static ManualResetEvent waitHandle;

        static InternalState()
        {
            waitHandle = new ManualResetEvent(false);
        }

        internal static ManualResetEvent WaitHandle
        {
            get
            {
                return waitHandle;
            }
        }

        private static ExecutorConfiguration executorConfiguration;
        internal static ExecutorConfiguration ExecutorConfiguration
        {
            get 
            {
                ExecutorConfiguration temp = null;
                lock (syncLock)
                {
                    temp = executorConfiguration;
                }
                return temp;
            }
            set 
            {
                if (value == null)
                    throw new ArgumentNullException("executorConfiguration");

                lock (syncLock)
                {
                    //set it only the first time
                    if (executorConfiguration == null)
                        executorConfiguration = value;
                    else
                        throw new InvalidOperationException("The executor configuration has already been set.");
                }
            }
        }

        /// <summary>
        /// This proxy is created by the resource manager, after it successfully registers with the Manager using a valid url.
        /// </summary>
        private static IJobManager jmProxy;
        internal static IJobManager JobManager
        {
            get
            {
                IJobManager jmp = null;
                lock (syncLock)
                {
                    jmp = jmProxy;
                }
                return jmp;
            }
            set
            {
                lock (syncLock)
                {
                    jmProxy = value;
                }
            }
        }

        /// <summary>
        /// This proxy is created by the resource manager, after it successfully registers with the Manager using a valid url.
        /// </summary>
        private static IDataTransferService dtProxy;
        internal static IDataTransferService DataTransferService
        {
            get
            {
                IDataTransferService dts = null;
                lock (syncLock)
                {
                    dts = dtProxy;
                }
                return dts;
            }
            set
            {
                lock (syncLock)
                {
                    dtProxy = value;
                }
            }
        }
    }
}
