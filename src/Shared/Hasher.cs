using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Reflection;

namespace Utilify.Platform
{
    internal static class Hasher
    {
        internal static string ComputeHash(string input, HashType algorithm)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            byte[] inputBytes = Encoding.Unicode.GetBytes(input); //UTF-16
            
            byte[] hash = null;
            HashAlgorithm hasher = GetHasher(algorithm);
            using (hasher)
            {
                hash = hasher.ComputeHash(inputBytes);
            }

            return GetHexString(hash);
        }

        internal static string ComputeHash(FileInfo file, HashType algorithm)
        {
            if (file == null)
                throw new ArgumentNullException("file");

            string hashValue = null;
            using (FileStream fs = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                hashValue = ComputeHash(fs, algorithm);
            }
            return hashValue;
        }

        internal static string ComputeHash(Stream inputStream, HashType algorithm)
        {
            if (inputStream == null)
                throw new ArgumentNullException("inputStream");

            byte[] hash = null;
            HashAlgorithm hasher = GetHasher(algorithm);
            using (hasher)
            {
               hash = hasher.ComputeHash(inputStream);
            }

            return GetHexString(hash);
        }

        /// <summary>
        /// Gets a hexadecimal string representation of the input bytes. (lowercase)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string GetHexString(byte[] input)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < input.Length; i++)
            {
                sb.Append(input[i].ToString("x2"));
            }
            return sb.ToString().ToLower();
        }

        private static HashAlgorithm GetHasher(HashType algorithm)
        {
            HashAlgorithm hasher = null;
            switch (algorithm)
            {
                case HashType.MD5:
                    hasher = new MD5CryptoServiceProvider();
                    break;
                case HashType.SHA1:
                    hasher = new SHA1Managed();
                    break;
                case HashType.SHA256:
                    hasher = new SHA256Managed();
                    break;
                case HashType.SHA384:
                    hasher = new SHA384Managed();
                    break;
                case HashType.SHA512:
                    hasher = new SHA512Managed();
                    break;
            }
            return hasher;
        }
    }

    internal enum HashType
    {
        MD5 = -2,
        SHA1 = -1,
        SHA256 = 0, //default
        SHA384 = 1,
        SHA512 = 2
    }
}
