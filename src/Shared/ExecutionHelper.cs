using System;

namespace Utilify.Platform.Execution.Shared
{
    internal static class ExecutionHelper
    {
        public const string ExecutionAssemblyFileName = "Utilify.Platform.Execution.dll";

        public static string GetExecutorTypeName(JobType type)
        {
            String typeName = null;

            // Consider that ClientPlatform == DotNet
            switch (type)
            {
                case JobType.CodeModule:
                    typeName = "Utilify.Platform.Execution.ClrExecutor";
                    break;
                case JobType.NativeModule:
                    typeName = "Utilify.Platform.Execution.NativeExecutor";
                    break;
            }
            return typeName;
        }

        public static string GetExecutionAssemblyLocation(AppDomain domain)
        {
            if (domain == null)
                throw new ArgumentNullException("domain");
            return System.IO.Path.Combine(domain.SetupInformation.ApplicationBase, ExecutionAssemblyFileName);
        }
    }
}
