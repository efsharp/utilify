using System;

namespace Utilify.Platform
{
    /// <summary>
    /// The BackOffHelper is a utility class used to get the next time interval to wait,
    /// when an operation fails. (eg. a network call fails and the caller needs to wait for some time before trying again).
    /// The GetNextInterval() call, along with CurrentInterval property return the amount of time to wait.
    /// The BackOffHelper is only used to keep track of the time intervals and number of retries - it has nothing 
    /// to do with the actual operation the succeeded/failed.
    /// 
    /// The maximum number of re-tries is the value till which the interval keeps increasing.
    /// That means there is nothing stopping the caller from calling GetNextInterval, even after
    /// the max # retries has been reached. The interval returned will remain the same after the max retries/ max interval is reached.
    /// but the number of retries is still incremented, to reflect the # of times GetNextInterval was called.
    /// This is the behaviour by design.
    /// </summary>
    internal class BackOffHelper
    {
        //Exponential back-off helper methods for threads
        internal const int DefaultMinWaitInterval = 10; //10 milliseconds
        internal const int DefaultMaxWaitInterval = 1000 * (2 * 60 * 60); //2 hours
        internal const int DefaultMaxRetries = int.MaxValue;

        private int maxRetries = DefaultMaxRetries;
        private int maxInterval = DefaultMaxWaitInterval;

        private int currentInterval = DefaultMinWaitInterval;
        private int numberOfRetries = 1;

        private int initialInterval = DefaultMinWaitInterval;

        internal BackOffHelper() : this(DefaultMinWaitInterval, DefaultMaxWaitInterval, DefaultMaxRetries)
        {
        }

        internal BackOffHelper(int maxRetries) : this(DefaultMinWaitInterval, DefaultMaxWaitInterval, maxRetries)
        {
        }

        internal BackOffHelper(int currentInterval, int maxInterval) : this(currentInterval, maxInterval, DefaultMaxRetries)
        {
        }

        //need to have a long, since we can't have two overloads with the same param types and order
        internal BackOffHelper(int currentInterval, long maxRetries)
            : this(currentInterval, DefaultMaxWaitInterval, (int)maxRetries)
        {
        }

        //todoFix: need to modify this to take only two parameters: taking these three parameters is meaning-less.
        private BackOffHelper(int currentInterval, int maxInterval, int maxRetries)
        {
            this.CurrentInterval = currentInterval;            
            this.MaxInterval = maxInterval;
            this.MaxRetries = maxRetries;

            this.initialInterval = this.CurrentInterval;
        }

        /// <summary>
        /// Gets the maximum number of re-tries till which the interval keeps increasing
        /// </summary>
        internal int MaxRetries
        {
            get
            {
                return maxRetries;
            }
            private set
            {
                if (value <= 0)
                    maxRetries = DefaultMaxRetries;
                else
                    maxRetries = value;
            }
        }

        /// <summary>
        /// Gets the maximum internval value (milliseconds) till which the interval keeps increasing
        /// </summary>
        internal int MaxInterval
        {
            get
            {
                return maxInterval;
            }
            private set
            {
                if (value <= 0)
                    maxInterval = DefaultMaxWaitInterval;
                else
                    maxInterval = value;

                //make sure the max interval is atleast as much as the currentInterval value
                if (maxInterval < currentInterval)
                    maxInterval = currentInterval;
            }
        }

        /// <summary>
        /// Gets the current interval value (in milliseconds)
        /// </summary>
        internal int CurrentInterval
        {
            get
            {
                return currentInterval;
            }
            private set
            {
                if (value <= 0)
                    currentInterval = DefaultMinWaitInterval;
                else
                    currentInterval = value;

                //make sure the max interval is atleast as much as the currentInterval value
                if (maxInterval < currentInterval)
                    maxInterval = currentInterval;
            }
        }

        /// <summary>
        /// The number of times GetNextInterval was called
        /// </summary>
        internal int NumberOfRetries
        {
            get
            {
                return numberOfRetries;
            }
        }

        /// <summary>
        /// Gets the next interval to wait for, before attempting an operation (that could possibly fail).
        /// The interval returned is the number of milliseconds to wait.
        /// </summary>
        /// <returns></returns>
        public int GetNextInterval()
        {
            int interval = this.CurrentInterval;

            interval = initialInterval * (int)Math.Pow(2, numberOfRetries);
            
            //this can happen if the interval gets so high that it goes beyond the Int32 range, and rolls over.
            if (interval <= 0)
                interval = int.MaxValue;

            numberOfRetries++; //increment # of retries

            if (interval > MaxInterval)
                interval = MaxInterval;

            this.CurrentInterval = interval;

            return interval;
        }

        public void Reset()
        {
            this.CurrentInterval = this.initialInterval;
            this.numberOfRetries = 0;
        }

        public bool IsWithinMaxLimits
        {
            get
            {
                return (NumberOfRetries <= MaxRetries && CurrentInterval < MaxInterval);
            }
        }
    }
}
