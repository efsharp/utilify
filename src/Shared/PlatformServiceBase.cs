﻿using System;
using System.Threading;

namespace Utilify.Platform
{
    /// <summary>
    /// ServiceStop options:
    /// Default : will indicate to the service that it is to stop normally.
    /// ForceAbort: will indicate to the service that it is to abort / stop immediately
    /// </summary>
    public enum ServiceStopOptions
    {
        Default = 0,
        ForceAbort = 1
    }

    internal interface IService
    {
        bool IsRunning { get; }
        void Start();
        void Stop();
        void Stop(ServiceStopOptions options);
        //todoLater: may need some method to ReloadConfiguration(); ?
    }

    internal abstract class PlatformServiceBase : IService
    {
        protected const int DefaultSleepInterval = 1000;
        protected readonly Logger logger;
        protected Thread thread;

        private readonly object synclock = new object();
        private bool stopFlag = false;
        private BackOffHelper backOff;

        public event EventHandler<ServiceErrorEventArgs> Error;

        protected virtual BackOffHelper BackOff
        {
            get { return backOff; }
            set { backOff = value; }
        }

        protected internal abstract string Name { get; }

        protected PlatformServiceBase() : this(DefaultSleepInterval)
        {
        }

        protected PlatformServiceBase(int currentInterval)
        {
            logger = new Logger();
            backOff = new BackOffHelper(currentInterval, BackOffHelper.DefaultMaxWaitInterval);
        }

        #region IService members

        public bool IsRunning
        {
            get 
            {
                lock (synclock)
                {
                    return (thread == null) ? false : thread.IsAlive;
                }
            }
        }

        public void Start()
        {
            lock (synclock)
            {
                if (IsRunning)
                    return;

                stopFlag = false;
                thread = new Thread(new ThreadStart(Run));
                thread.Name = this.Name; //can't use GetType() here when it is obfuscated - we get a meaningless log.
                thread.IsBackground = true;
                thread.Start();
                logger.Info("Started service: {0} ", this.Name); //can't use GetType() here when it is obfuscated - we get a meaningless log.
            }
        }

        /// <summary>
        /// Requests the service to stop. 
        /// (Note to implementers: The derived classes which have a long running thread should check the IsStopRequested flag,
        /// and gracefully stop processing to end the thread.)
        /// </summary>
        public void Stop()
        {
            Stop(ServiceStopOptions.Default);
        }

        /// <summary>
        /// Requests the service to stop with the specified options.
        /// </summary>
        /// <param name="options"></param>
        public void Stop(ServiceStopOptions options)
        {
            if (stopFlag)
                return;

            lock (synclock)
            {
                stopFlag = true;
            }
            if (options == ServiceStopOptions.ForceAbort && thread.IsAlive)
            {
                //also force:
                thread.Abort();
                thread = null;
            }
            logger.Info("Stopping service: {0} ", this.Name);
            
        }

        #endregion

        protected bool IsStopRequested
        {
            get
            {
                bool isRequested = false;
                lock (synclock)
                {
                    isRequested = stopFlag;
                }
                return isRequested;
            }
        }

        protected virtual void OnError(Exception error)
        {
            OnError(error, false);
        }

        protected virtual void OnError(Exception error, bool isTerminating)
        {
            logger.Warn("Raising OnError ", error);
            ServiceErrorEventArgs args = new ServiceErrorEventArgs(error, isTerminating);
            EventHelper.RaiseEvent<ServiceErrorEventArgs>(Error, this, args);
        }

        /// <summary>
        /// The 'Run' method is run on a seperate thread.
        /// This base class has some common code to help derived-classes implement long running threads.
        /// Typically the derived class will run a while-loop checking every so often if it has been asked to stop.
        /// </summary>
        protected abstract void Run();
    }

    internal class ServiceErrorEventArgs : EventArgs
    {
        private Exception error;
        private bool isTerminating;

        public ServiceErrorEventArgs() : base() { }

        public ServiceErrorEventArgs(Exception error, bool isTerminating)
            : base()
        {
            this.error = error;
            this.isTerminating = isTerminating;
        }

        public Exception Error
        {
            get { return error; }
            set { error = value; }
        }

        public bool IsTerminating
        {
            get { return isTerminating; }
            set { isTerminating = true; }
        }
    }
}
