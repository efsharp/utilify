using System;
using System.IO;
using Utilify.Platform;

namespace Utilify.Platform.Execution.Shared
{
    internal static class MessageDataHelper 
    {
        private static readonly Logger logger = new Logger();

        public static bool SendMessage(Stream stream, IMessageData msgData)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            if (msgData == null)
                throw new ArgumentNullException("msgData");

            bool success = true;

            hessiancsharp.io.CHessianOutput output = new hessiancsharp.io.CHessianOutput(stream);
            output.WriteObject(msgData);
            
            return success;
        }

        public static IMessageData ReceiveMessage(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            //set defaults
            IMessageData msgData = null;

            hessiancsharp.io.CHessianInput input = new hessiancsharp.io.CHessianInput(stream);
            object read = input.ReadObject();

            msgData = read as IMessageData;

            return msgData;
        }

    }
}
